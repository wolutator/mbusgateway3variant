#!/bin/bash

cat << EOF | nc localhost 4444
reset halt
flash write_image erase /home/wn/Workspaces/mbusgateway3variant/cube/build/cube.elf
reset run
exit
EOF
