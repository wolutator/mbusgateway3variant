#!/bin/bash

SESSIONNAME=mbusmaster-dev

screen -d -m -S $SESSIONNAME
screen -S $SESSIONNAME -p 0 -X title main

screen -S $SESSIONNAME -X screen 1
screen -S $SESSIONNAME -p 1 -X title build
screen -S $SESSIONNAME -p 1 -X exec ./tools/startBuildEnv.sh

screen -S $SESSIONNAME -X screen 2
screen -S $SESSIONNAME -p 2 -X title test
screen -S $SESSIONNAME -p 2 -X exec ./tools/startTestEnv.sh

screen -S $SESSIONNAME -X screen 3
screen -S $SESSIONNAME -p 3 -X title minicom
screen -S $SESSIONNAME -p 3 -X exec minicom

screen -S $SESSIONNAME -X screen 4
screen -S $SESSIONNAME -p 4 -X title openocd
screen -S $SESSIONNAME -p 4 -X exec openocd -f ./openocd.cfg

screen -S $SESSIONNAME -X screen 5
screen -S $SESSIONNAME -p 5 -X title upload

screen -S $SESSIONNAME -X screen 6
screen -S $SESSIONNAME -p 6 -X title edit
screen -S $SESSIONNAME -p 6 -X chdir ./cube

screen -r $SESSIONNAME

