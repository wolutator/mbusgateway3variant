#!/bin/bash

MAIN_C=./Core/Src/main.c
MAIN_C_BAK=${MAIN_C}-bak

IT_C=./Core/Src/stm32f1xx_it.c
IT_C_BAK=${IT_C}-bak

MAKEFILE=./Makefile
MAKEFILE_BAK=${MAKEFILE}-bak

PROCESSED="Processed by $0"

checkFile () {
  FILE=$1
  BAK_FILE=$2
  if [ ! -f $FILE ]; then
    echo "no $FILE available"
    exit 1
  fi
  if [ -f $BAK_FILE ]; then
    echo "$BAK_FILE already available, delete manually"
    exit 1
  fi
  grep -q "$PROCESSED" $FILE
  if [ "$?" = "0" ]; then
    echo "$FILE has already been processed"
    exit 1
  fi
}


checkFile $MAIN_C $MAIN_C_BAK
checkFile $MAKEFILE $MAKEFILE_BAK
checkFile $IT_C $IT_C_BAK

cp $MAIN_C $MAIN_C_BAK
echo "// $PROCESSED" > $MAIN_C
cat $MAIN_C_BAK | \
  sed -e 's,\(/\* USER CODE BEGIN Includes \*/\),\1\n#include "main2.h"\n,' | \
  sed -e 's,\(/\* USER CODE BEGIN 1 \*/\),\1\n  my_setup_1();\n,' | \
  sed -e 's,\(/\* USER CODE BEGIN 2 \*/\),\1\n  my_setup_2();\n,' | \
  sed -e 's,\(/\* USER CODE BEGIN 3 \*/\),\1\n    my_loop();\n,' | \
  sed -e 's,\(/\* USER CODE BEGIN Error_Handler_Debug \*/\),\1\n  my_errorHandler();\n,' | \
  sed -e 's,\(/\* USER CODE END Error_Handler_Debug \*/\),\1\n  while(1) { };\n,' >> $MAIN_C

cp $IT_C $IT_C_BAK
echo "// $PROCESSED" > $IT_C
cat $IT_C_BAK | \
  sed -e 's,\(/\* USER CODE BEGIN Includes \*/\),\1\n#include "main2.h"\n,' \
      -e 's,\(/\* USER CODE BEGIN PFP \*/\),\1\nvoid mbusCommISR();\n,' \
      -e 's,\(/\* USER CODE BEGIN SysTick_IRQn 1 \*/\),\1\n  SYSTICK_Callback();\n,' \
      -e 's,\(HAL_UART_IRQHandler(&huart5);\),// \1,' \
      -e 's,\(/\* USER CODE BEGIN UART5_IRQn 1 \*/\),\1\n  mbusCommISR();\n,' \
      >> $IT_C
  
# mkdir w5500
# pushd ioLibrary_Driver  
# for D in Ethernet Ethernet/W5500 Internet/DHCP Internet/DNS Internet/httpServer Internet/MQTT; do
#    cp $D/*.c $D/*.h ../w5500
# done
# popd


SRC_EXT=''
for I in User/Src/*.c; do
  SRC_EXT+="$I "
done
for I in hottislib/*.c; do
  SRC_EXT+="$I "
done
# for I in w5500/*.c; do
#   SRC_EXT+="$I "
# done

cp $MAKEFILE $MAKEFILE_BAK
echo "# $PROCESSED" > $MAKEFILE
cat $MAKEFILE_BAK | \
  sed -e 's/\(-specs=nano.specs\)/\1 -u _printf_float/' | \
  sed -e 's/\(-Wall\)/\1 -Werror/' | \
  sed -e 's%\(# list of ASM program objects\)%OBJECTS += $(addprefix $(BUILD_DIR)/,w5500.a pubsubc.a)\n\1%' | \
  sed -e 's,\($(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR)\),$(BUILD_DIR)/w5500.a:\n\t(cd ioLibrary_Driver \&\& $(MAKE) \&\& cp w5500.a ../$(BUILD_DIR) \&\& cd ..)\n\n\1,' | \
  sed -e 's,\($(BUILD_DIR)/%.o: %.c Makefile | $(BUILD_DIR)\),$(BUILD_DIR)/pubsubc.a:\n\t(cd pubsubc \&\& $(MAKE) \&\& cp pubsubc.a ../$(BUILD_DIR) \&\& cd ..)\n\n\1,' | \
  sed -e 's,\(C_SOURCES =  \\\),\1\nlibmbus/mbus/mbus-protocol.c \\,' | \
  sed -e 's,\(C_SOURCES =  \\\),\1\n'"$SRC_EXT"' \\,' | \
  sed -e 's,\(C_INCLUDES =  \\\),\1\n-IioLibrary_Driver/Ethernet \\,' | \
  sed -e 's,\(C_INCLUDES =  \\\),\1\n-IioLibrary_Driver/Internet/DHCP \\,' | \
  sed -e 's,\(C_INCLUDES =  \\\),\1\n-IioLibrary_Driver/Internet/DNS \\,' | \
  sed -e 's,\(C_INCLUDES =  \\\),\1\n-IUser/Inc \\,' | \
  sed -e 's,\(C_INCLUDES =  \\\),\1\n-Ilibmbus \\,' | \
  sed -e 's,\(C_INCLUDES =  \\\),\1\n-Ipubsubc/src \\,' | \
  sed -e 's,\(C_INCLUDES =  \\\),\1\n-Ihottislib \\,' >> $MAKEFILE

#  sed -e 's,\(C_INCLUDES =  \\\),\1\n-Iw5500 \\,' | \




	