## Build environment

On Ubuntu/Debian install

* ``gcc-arm-none-eabi``
* ``openocd``


Alternatively use a prepared Docker image

    docker run -it --rm -u ${UID} -v ${PWD}:/mnt wollud1969/build-env-arm-none-eabi:1.1.0 bash

Start the container from the project root. Or use the script ``startBuildEnv.sh`` from the tools directory.


Get the STM32CubeMX tool from the ST homepage. You may use this [link](https://www.st.com/en/development-tools/stm32cubemx.html).
    

## Preparing generated code to include user code

After code generation in CubeMX the ``Makefile`` and the file ``main.c`` in ``Core/Src`` needs
to be edited to include user code. From the ``cube`` directory in the project root run:

    ../tools/insertMyCode.sh

User code has to life in ``User/Src`` and ``User/Inc``. All ``*.c`` file from ``User/Src`` will be
attached to the ``C_SOURCES`` variable in the ``Makefile``.


## Building

From the ``cube`` directory in the project root simply run

    make


## Uploading to the target

Start ``openocd`` from the project root using the existing configuration file (for derived projects
adjust the configuration concerning the use MCU)

    openocd -f openocd.cfg

Connect to the started ``openocd`` server using

    nc localhost 4444

To upload a built binary enter

    reset init
    reset halt
    flash write_image erase /home/wn/Workspaces/$PROJECT/cube/build/cube.elf
    reset run

Make sure to use the absolute path. Alternatively, use the script ``upload.sh`` from the tools directory.






    
