#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stddef.h>

typedef struct {
  uint8_t id;
  char a[16];
  uint8_t length;
  uint32_t x;
} t_testObj;
  
t_testObj testObj = { .id = 0, .a = "test123", .length = 25, .x = 123456 };

void printTestObj(t_testObj *testObj) {
  printf("testObj: id=%d, a=%s, length=%d, x=%d\n", testObj->id, testObj->a, testObj->length, testObj->x);
}

int main() {
  printf("id:     %ld\n", offsetof(t_testObj, id));
  printf("a:      %ld\n", offsetof(t_testObj, a));
  printf("length: %ld\n", offsetof(t_testObj, length));
  printf("x:      %ld\n", offsetof(t_testObj, x));

  printTestObj(&testObj);

  strcpy(testObj.a, "demo234");
  printTestObj(&testObj);

  uint8_t *a1 = (uint8_t*)&testObj;
  printf("a1: %p\n", a1);

  uint8_t *a2 = ((uint8_t*)&testObj) + offsetof(t_testObj, length);
  printf("a2: %p\n", a2);

  *a2 = 35;
  printTestObj(&testObj);

  uint8_t *a3 = ((uint8_t*)&testObj) + offsetof(t_testObj, a);
  printf("a3: %p\n", a3);

  strcpy((char*)a3, "Wolfgang");
  printTestObj(&testObj);

  uint32_t *a4 = (uint32_t*) (((uint8_t*)&testObj) + offsetof(t_testObj, x));
  printf("a4: %p\n", a4);

  *a4 = 1234567890;
  printTestObj(&testObj);
}




