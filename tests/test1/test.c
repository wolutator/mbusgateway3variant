#include <CUnit/Basic.h>
#include <stdio.h>
#include <stdbool.h>
#include <ringbuffer.h>
//#include <logger.h>


// #define DEBUG

ringbuffer_t rb;



void printRingbuffer(ringbuffer_t *rb) {
    printf("Ringbuffer:\n");
    printf("  Size:     %u\n", rb->bufferSize);
    printf("  ReadIdx:  %u\n", rb->bufferReadIdx);
    printf("  WriteIdx: %u\n", rb->bufferWriteIdx);

    for (uint32_t i = 0; i < rb->bufferSize; i++) {
        printf("%02x ", rb->buffer[i]);
    }
    printf("\n");
}

int init_suite_ringbuffer(void) {
    return 0;
}

int clean_suite_ringbuffer(void) {
    return 0;
}


void testRingbuffer0() {
#ifdef DEBUG    
    printf("Initialize ringbuffer\n");
#endif
    ringbufferInit(&rb, 16);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 0);
    CU_ASSERT(rb.bufferReadIdx == 0);

    CU_ASSERT(rb.buffer[0] == 0);
    CU_ASSERT(rb.buffer[1] == 0);
    CU_ASSERT(rb.buffer[2] == 0);
    CU_ASSERT(rb.buffer[3] == 0);
    CU_ASSERT(rb.buffer[4] == 0);
    CU_ASSERT(rb.buffer[5] == 0);
    CU_ASSERT(rb.buffer[6] == 0);
    CU_ASSERT(rb.buffer[7] == 0);
    CU_ASSERT(rb.buffer[8] == 0);
    CU_ASSERT(rb.buffer[9] == 0);
    CU_ASSERT(rb.buffer[10] == 0);
    CU_ASSERT(rb.buffer[11] == 0);
    CU_ASSERT(rb.buffer[12] == 0);
    CU_ASSERT(rb.buffer[13] == 0);
    CU_ASSERT(rb.buffer[14] == 0);
    CU_ASSERT(rb.buffer[15] == 0);
}

void testRingbuffer1() {
#ifdef DEBUG
    printf("\nPut 5 chars in buffer\n");
#endif
    int r = ringbufferPut(&rb, "abcde", 5);
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 5);
    CU_ASSERT(rb.bufferReadIdx == 0);

    CU_ASSERT(rb.buffer[0] == 'a');
    CU_ASSERT(rb.buffer[1] == 'b');
    CU_ASSERT(rb.buffer[2] == 'c');
    CU_ASSERT(rb.buffer[3] == 'd');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 0);
    CU_ASSERT(rb.buffer[6] == 0);
    CU_ASSERT(rb.buffer[7] == 0);
    CU_ASSERT(rb.buffer[8] == 0);
    CU_ASSERT(rb.buffer[9] == 0);
    CU_ASSERT(rb.buffer[10] == 0);
    CU_ASSERT(rb.buffer[11] == 0);
    CU_ASSERT(rb.buffer[12] == 0);
    CU_ASSERT(rb.buffer[13] == 0);
    CU_ASSERT(rb.buffer[14] == 0);
    CU_ASSERT(rb.buffer[15] == 0);
}

void testRingbuffer2() {
#ifdef DEBUG
    printf("\nPut 5 chars in buffer\n");
#endif
    int r = ringbufferPut(&rb, "fghij", 5);
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 10);
    CU_ASSERT(rb.bufferReadIdx == 0);

    CU_ASSERT(rb.buffer[0] == 'a');
    CU_ASSERT(rb.buffer[1] == 'b');
    CU_ASSERT(rb.buffer[2] == 'c');
    CU_ASSERT(rb.buffer[3] == 'd');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 'f');
    CU_ASSERT(rb.buffer[6] == 'g');
    CU_ASSERT(rb.buffer[7] == 'h');
    CU_ASSERT(rb.buffer[8] == 'i');
    CU_ASSERT(rb.buffer[9] == 'j');
    CU_ASSERT(rb.buffer[10] == 0);
    CU_ASSERT(rb.buffer[11] == 0);
    CU_ASSERT(rb.buffer[12] == 0);
    CU_ASSERT(rb.buffer[13] == 0);
    CU_ASSERT(rb.buffer[14] == 0);
    CU_ASSERT(rb.buffer[15] == 0);
}

void testRingbuffer3() {
#ifdef DEBUG
    printf("\nPut 5 chars in buffer\n");
#endif
    int r = ringbufferPut(&rb, "klmno", 5);
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 15);
    CU_ASSERT(rb.bufferReadIdx == 0);

    CU_ASSERT(rb.buffer[0] == 'a');
    CU_ASSERT(rb.buffer[1] == 'b');
    CU_ASSERT(rb.buffer[2] == 'c');
    CU_ASSERT(rb.buffer[3] == 'd');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 'f');
    CU_ASSERT(rb.buffer[6] == 'g');
    CU_ASSERT(rb.buffer[7] == 'h');
    CU_ASSERT(rb.buffer[8] == 'i');
    CU_ASSERT(rb.buffer[9] == 'j');
    CU_ASSERT(rb.buffer[10] == 'k');
    CU_ASSERT(rb.buffer[11] == 'l');
    CU_ASSERT(rb.buffer[12] == 'm');
    CU_ASSERT(rb.buffer[13] == 'n');
    CU_ASSERT(rb.buffer[14] == 'o');
    CU_ASSERT(rb.buffer[15] == 0);
}

void testRingbuffer4() {
#ifdef DEBUG
    printf("\nPut 5 chars in buffer\n");
#endif
    int r = ringbufferPut(&rb, "pqrst", 5);
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif
    CU_ASSERT(r == -1);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 15);
    CU_ASSERT(rb.bufferReadIdx == 0);

    CU_ASSERT(rb.buffer[0] == 'a');
    CU_ASSERT(rb.buffer[1] == 'b');
    CU_ASSERT(rb.buffer[2] == 'c');
    CU_ASSERT(rb.buffer[3] == 'd');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 'f');
    CU_ASSERT(rb.buffer[6] == 'g');
    CU_ASSERT(rb.buffer[7] == 'h');
    CU_ASSERT(rb.buffer[8] == 'i');
    CU_ASSERT(rb.buffer[9] == 'j');
    CU_ASSERT(rb.buffer[10] == 'k');
    CU_ASSERT(rb.buffer[11] == 'l');
    CU_ASSERT(rb.buffer[12] == 'm');
    CU_ASSERT(rb.buffer[13] == 'n');
    CU_ASSERT(rb.buffer[14] == 'o');
    CU_ASSERT(rb.buffer[15] == 0);
}

void testRingbuffer5() {
#ifdef DEBUG
    printf("\nRead one char from buffer\n");
#endif
    int r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 15);
    CU_ASSERT(rb.bufferReadIdx == 1);

    uint8_t rr = (uint8_t) r;
    CU_ASSERT(rr == 'a');

    CU_ASSERT(rb.buffer[0] == 'a');
    CU_ASSERT(rb.buffer[1] == 'b');
    CU_ASSERT(rb.buffer[2] == 'c');
    CU_ASSERT(rb.buffer[3] == 'd');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 'f');
    CU_ASSERT(rb.buffer[6] == 'g');
    CU_ASSERT(rb.buffer[7] == 'h');
    CU_ASSERT(rb.buffer[8] == 'i');
    CU_ASSERT(rb.buffer[9] == 'j');
    CU_ASSERT(rb.buffer[10] == 'k');
    CU_ASSERT(rb.buffer[11] == 'l');
    CU_ASSERT(rb.buffer[12] == 'm');
    CU_ASSERT(rb.buffer[13] == 'n');
    CU_ASSERT(rb.buffer[14] == 'o');
    CU_ASSERT(rb.buffer[15] == 0);
}

void testRingbuffer6() {
#ifdef DEBUG
    printf("\nRead some char from buffer\n");
#endif
    int r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 15);
    CU_ASSERT(rb.bufferReadIdx == 2);

    uint8_t rr = (uint8_t) r;
    CU_ASSERT(rr == 'b');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 15);
    CU_ASSERT(rb.bufferReadIdx == 3);

    rr = (uint8_t) r;
    CU_ASSERT(rr == 'c');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 15);
    CU_ASSERT(rb.bufferReadIdx == 4);

    rr = (uint8_t) r;
    CU_ASSERT(rr == 'd');

    CU_ASSERT(rb.buffer[0] == 'a');
    CU_ASSERT(rb.buffer[1] == 'b');
    CU_ASSERT(rb.buffer[2] == 'c');
    CU_ASSERT(rb.buffer[3] == 'd');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 'f');
    CU_ASSERT(rb.buffer[6] == 'g');
    CU_ASSERT(rb.buffer[7] == 'h');
    CU_ASSERT(rb.buffer[8] == 'i');
    CU_ASSERT(rb.buffer[9] == 'j');
    CU_ASSERT(rb.buffer[10] == 'k');
    CU_ASSERT(rb.buffer[11] == 'l');
    CU_ASSERT(rb.buffer[12] == 'm');
    CU_ASSERT(rb.buffer[13] == 'n');
    CU_ASSERT(rb.buffer[14] == 'o');
    CU_ASSERT(rb.buffer[15] == 0);
}

void testRingbuffer7() {
#ifdef DEBUG
    printf("\nPut 5 chars in buffer\n");
#endif
    int r = ringbufferPut(&rb, "1234", 4);
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 3);
    CU_ASSERT(rb.bufferReadIdx == 4);

    CU_ASSERT(rb.buffer[0] == '2');
    CU_ASSERT(rb.buffer[1] == '3');
    CU_ASSERT(rb.buffer[2] == '4');
    CU_ASSERT(rb.buffer[3] == 'd');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 'f');
    CU_ASSERT(rb.buffer[6] == 'g');
    CU_ASSERT(rb.buffer[7] == 'h');
    CU_ASSERT(rb.buffer[8] == 'i');
    CU_ASSERT(rb.buffer[9] == 'j');
    CU_ASSERT(rb.buffer[10] == 'k');
    CU_ASSERT(rb.buffer[11] == 'l');
    CU_ASSERT(rb.buffer[12] == 'm');
    CU_ASSERT(rb.buffer[13] == 'n');
    CU_ASSERT(rb.buffer[14] == 'o');
    CU_ASSERT(rb.buffer[15] == '1');
}

void testRingbuffer8() {
#ifdef DEBUG
    printf("\nPut 5 chars in buffer\n");
#endif
    int r = ringbufferPut(&rb, "67890", 5);
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == -1);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 3);
    CU_ASSERT(rb.bufferReadIdx == 4);

    CU_ASSERT(rb.buffer[0] == '2');
    CU_ASSERT(rb.buffer[1] == '3');
    CU_ASSERT(rb.buffer[2] == '4');
    CU_ASSERT(rb.buffer[3] == 'd');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 'f');
    CU_ASSERT(rb.buffer[6] == 'g');
    CU_ASSERT(rb.buffer[7] == 'h');
    CU_ASSERT(rb.buffer[8] == 'i');
    CU_ASSERT(rb.buffer[9] == 'j');
    CU_ASSERT(rb.buffer[10] == 'k');
    CU_ASSERT(rb.buffer[11] == 'l');
    CU_ASSERT(rb.buffer[12] == 'm');
    CU_ASSERT(rb.buffer[13] == 'n');
    CU_ASSERT(rb.buffer[14] == 'o');
    CU_ASSERT(rb.buffer[15] == '1')
}

void testRingbuffer9() {
#ifdef DEBUG
    printf("\nRead one char from buffer\n");
#endif
    int r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 3);
    CU_ASSERT(rb.bufferReadIdx == 5);

    uint8_t rr = (uint8_t) r;
    CU_ASSERT(rr == 'e');

    CU_ASSERT(rb.buffer[0] == '2');
    CU_ASSERT(rb.buffer[1] == '3');
    CU_ASSERT(rb.buffer[2] == '4');
    CU_ASSERT(rb.buffer[3] == 'd');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 'f');
    CU_ASSERT(rb.buffer[6] == 'g');
    CU_ASSERT(rb.buffer[7] == 'h');
    CU_ASSERT(rb.buffer[8] == 'i');
    CU_ASSERT(rb.buffer[9] == 'j');
    CU_ASSERT(rb.buffer[10] == 'k');
    CU_ASSERT(rb.buffer[11] == 'l');
    CU_ASSERT(rb.buffer[12] == 'm');
    CU_ASSERT(rb.buffer[13] == 'n');
    CU_ASSERT(rb.buffer[14] == 'o');
    CU_ASSERT(rb.buffer[15] == '1')
}

void testRingbuffer10() {
#ifdef DEBUG
    printf("\nPut 1 chars in buffer\n");
#endif
    int r = ringbufferPut(&rb, "$", 1);
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 5);

    CU_ASSERT(rb.buffer[0] == '2');
    CU_ASSERT(rb.buffer[1] == '3');
    CU_ASSERT(rb.buffer[2] == '4');
    CU_ASSERT(rb.buffer[3] == '$');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 'f');
    CU_ASSERT(rb.buffer[6] == 'g');
    CU_ASSERT(rb.buffer[7] == 'h');
    CU_ASSERT(rb.buffer[8] == 'i');
    CU_ASSERT(rb.buffer[9] == 'j');
    CU_ASSERT(rb.buffer[10] == 'k');
    CU_ASSERT(rb.buffer[11] == 'l');
    CU_ASSERT(rb.buffer[12] == 'm');
    CU_ASSERT(rb.buffer[13] == 'n');
    CU_ASSERT(rb.buffer[14] == 'o');
    CU_ASSERT(rb.buffer[15] == '1')
}

void testRingbuffer11() {
#ifdef DEBUG
    printf("\nPut 1 chars in buffer\n");
#endif
    int r = ringbufferPut(&rb, "%", 1);
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == -1);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 5);

    CU_ASSERT(rb.buffer[0] == '2');
    CU_ASSERT(rb.buffer[1] == '3');
    CU_ASSERT(rb.buffer[2] == '4');
    CU_ASSERT(rb.buffer[3] == '$');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 'f');
    CU_ASSERT(rb.buffer[6] == 'g');
    CU_ASSERT(rb.buffer[7] == 'h');
    CU_ASSERT(rb.buffer[8] == 'i');
    CU_ASSERT(rb.buffer[9] == 'j');
    CU_ASSERT(rb.buffer[10] == 'k');
    CU_ASSERT(rb.buffer[11] == 'l');
    CU_ASSERT(rb.buffer[12] == 'm');
    CU_ASSERT(rb.buffer[13] == 'n');
    CU_ASSERT(rb.buffer[14] == 'o');
    CU_ASSERT(rb.buffer[15] == '1')
}

void testRingbuffer12() {
#ifdef DEBUG
    printf("\nRead unless the buffer is empty\n");
#endif
    int r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 6);

    uint8_t rr = (uint8_t) r;
    CU_ASSERT(rr == 'f');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 7);

    rr = (uint8_t) r;
    CU_ASSERT(rr == 'g');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 8);

    rr = (uint8_t) r;
    CU_ASSERT(rr == 'h');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 9);

    rr = (uint8_t) r;
    CU_ASSERT(rr == 'i');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 10);

    rr = (uint8_t) r;
    CU_ASSERT(rr == 'j');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 11);

    rr = (uint8_t) r;
    CU_ASSERT(rr == 'k');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 12);

    rr = (uint8_t) r;
    CU_ASSERT(rr == 'l');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 13);

    rr = (uint8_t) r;
    CU_ASSERT(rr == 'm');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 14);

    rr = (uint8_t) r;
    CU_ASSERT(rr == 'n');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 15);

    rr = (uint8_t) r;
    CU_ASSERT(rr == 'o');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 0);

    rr = (uint8_t) r;
    CU_ASSERT(rr == '1');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 1);

    rr = (uint8_t) r;
    CU_ASSERT(rr == '2');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 2);

    rr = (uint8_t) r;
    CU_ASSERT(rr == '3');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 3);

    rr = (uint8_t) r;
    CU_ASSERT(rr == '4');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r >= 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 4);

    rr = (uint8_t) r;
    CU_ASSERT(rr == '$');

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == -1);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 4);

    r = ringbufferGetOne(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == -1);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 4);
    CU_ASSERT(rb.bufferReadIdx == 4);

    CU_ASSERT(rb.buffer[0] == '2');
    CU_ASSERT(rb.buffer[1] == '3');
    CU_ASSERT(rb.buffer[2] == '4');
    CU_ASSERT(rb.buffer[3] == '$');
    CU_ASSERT(rb.buffer[4] == 'e');
    CU_ASSERT(rb.buffer[5] == 'f');
    CU_ASSERT(rb.buffer[6] == 'g');
    CU_ASSERT(rb.buffer[7] == 'h');
    CU_ASSERT(rb.buffer[8] == 'i');
    CU_ASSERT(rb.buffer[9] == 'j');
    CU_ASSERT(rb.buffer[10] == 'k');
    CU_ASSERT(rb.buffer[11] == 'l');
    CU_ASSERT(rb.buffer[12] == 'm');
    CU_ASSERT(rb.buffer[13] == 'n');
    CU_ASSERT(rb.buffer[14] == 'o');
    CU_ASSERT(rb.buffer[15] == '1')
}


void testRingbuffer13() {
#ifdef DEBUG
    printf("\nPut some chars in buffer\n");
#endif
    char t[] = "Hello World\n";
    int r = ringbufferPut(&rb, t, strlen(t));
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == 0);
}

void testRingbuffer14() {
#ifdef DEBUG
    printf("\nPut some chars in buffer\n");
#endif
    char t[] = "Hello World\n";
    int r = ringbufferPut(&rb, t, strlen(t));
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == -1);
}

void testRingbuffer15() {
#ifdef DEBUG
    printf("\nRead all chars from buffer\n");
#endif
    char goldValue[] = "Hello World\n";
    char buffer[20];
    memset(buffer, 0, 20);

    int c;
    uint8_t i = 0;
    while (0 < (c = ringbufferGetOne(&rb))) {
        buffer[i] = (uint8_t) c;
        i++;
    }

    CU_ASSERT(strcmp(goldValue, buffer) == 0);
}

void testRingbuffer16() {
#ifdef DEBUG
    printf("\nPut some chars in buffer\n");
#endif
    char goldValue[] = "Wolfgang\n";
    int r = ringbufferPut(&rb, goldValue, strlen(goldValue));
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == 0);

#ifdef DEBUG
    printf("\nRead all chars from buffer\n");
#endif
    char buffer[20];
    memset(buffer, 0, 20);

    int c;
    uint8_t i = 0;
    while (0 < (c = ringbufferGetOne(&rb))) {
        buffer[i] = (uint8_t) c;
        i++;
    }

    CU_ASSERT(strcmp(goldValue, buffer) == 0);
}


void testRingbuffer99() {
#ifdef DEBUG
    printf("De-Initialize ringbuffer\n");
#endif
    ringbufferFree(&rb);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(rb.buffer == NULL);
    CU_ASSERT(rb.bufferSize == 0);
    CU_ASSERT(rb.bufferWriteIdx == 0);
    CU_ASSERT(rb.bufferReadIdx == 0);
}


void testRingbuffer100() {
#ifdef DEBUG    
    printf("Initialize ringbuffer\n");
#endif
    ringbufferInit(&rb, 16);
#ifdef DEBUG
    printRingbuffer(&rb);
#endif

    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 0);
    CU_ASSERT(rb.bufferReadIdx == 0);

    CU_ASSERT(rb.buffer[0] == 0);
    CU_ASSERT(rb.buffer[1] == 0);
    CU_ASSERT(rb.buffer[2] == 0);
    CU_ASSERT(rb.buffer[3] == 0);
    CU_ASSERT(rb.buffer[4] == 0);
    CU_ASSERT(rb.buffer[5] == 0);
    CU_ASSERT(rb.buffer[6] == 0);
    CU_ASSERT(rb.buffer[7] == 0);
    CU_ASSERT(rb.buffer[8] == 0);
    CU_ASSERT(rb.buffer[9] == 0);
    CU_ASSERT(rb.buffer[10] == 0);
    CU_ASSERT(rb.buffer[11] == 0);
    CU_ASSERT(rb.buffer[12] == 0);
    CU_ASSERT(rb.buffer[13] == 0);
    CU_ASSERT(rb.buffer[14] == 0);
    CU_ASSERT(rb.buffer[15] == 0);
}


void testRingbuffer101() {
#ifdef DEBUG
    printf("\nPut 1 chars in buffer\n");
#endif
    int r = ringbufferPutOne(&rb, 'a');
#ifdef DEBUG
    printf("r = %d\n", r);
    printRingbuffer(&rb);
#endif

    CU_ASSERT(r == 0);
    CU_ASSERT(rb.buffer != NULL);
    CU_ASSERT(rb.bufferSize == 16);
    CU_ASSERT(rb.bufferWriteIdx == 1);
    CU_ASSERT(rb.bufferReadIdx == 0);

    CU_ASSERT(rb.buffer[0] == 'a');
    CU_ASSERT(rb.buffer[1] == 0);
    CU_ASSERT(rb.buffer[2] == 0);
    CU_ASSERT(rb.buffer[3] == 0);
    CU_ASSERT(rb.buffer[4] == 0);
    CU_ASSERT(rb.buffer[5] == 0);
    CU_ASSERT(rb.buffer[6] == 0);
    CU_ASSERT(rb.buffer[7] == 0);
    CU_ASSERT(rb.buffer[8] == 0);
    CU_ASSERT(rb.buffer[9] == 0);
    CU_ASSERT(rb.buffer[10] == 0);
    CU_ASSERT(rb.buffer[11] == 0);
    CU_ASSERT(rb.buffer[12] == 0);
    CU_ASSERT(rb.buffer[13] == 0);
    CU_ASSERT(rb.buffer[14] == 0);
    CU_ASSERT(rb.buffer[15] == 0);
}


/*
int init_suite_logger(void) {
    logInit();
    return 0;
}

int clean_suite_logger(void) {
    logFree();
    return 0;
}


void testLogger0() {
#ifdef DEBUG
    printf("\nLog a message\n");
#endif
    char msg[] = "Wolfgang";
    char goldValue[] = "Wolfgang\r\n";
    logMsg(msg);

#ifdef DEBUG
    printf("\nRead all chars from logging\n");
#endif
    char buffer[128];
    int c;
    uint8_t i = 0;

    memset(buffer, 0, 128);
    i = 0;
    while (0 < (c = logExec())) {
        buffer[i] = (uint8_t) c;
        i++;
    }
// printf(buffer);
    CU_ASSERT(strcmp(goldValue, buffer) == 0);
}

void testLogger1() {
#ifdef DEBUG
    printf("\nLog some messages\n");
#endif
    char goldValueFull[128];
    memset(goldValueFull, 0, 128);
    char goldValue1[] = "Wolfgang"; // 9
    strcat(goldValueFull, goldValue1);
    strcat(goldValueFull, "\r\n");
    logMsg(goldValue1);
    char goldValue2[] = "Andreas";  // +8 = 17
    strcat(goldValueFull, goldValue2);
    strcat(goldValueFull, "\r\n");
    logMsg(goldValue2);
    char goldValue3[] = "Frank";  // +6 = 23
    strcat(goldValueFull, goldValue3);
    strcat(goldValueFull, "\r\n");
    logMsg(goldValue3);

#ifdef DEBUG
    printf("\nRead all chars from logging\n");
#endif
    char buffer[128];
    int c;
    uint8_t i = 0;

    memset(buffer, 0, 128);
    i = 0;
    while (0 < (c = logExec())) {
        buffer[i] = (uint8_t) c;
        i++;
    }
    
    // printf("Buffer: %s\n", buffer);    
    CU_ASSERT(strcmp(goldValueFull, buffer) == 0);
}

void testLogger2() {
#ifdef DEBUG
    printf("\nLog some messages, a bit more than space in buffer\n");
#endif
    char goldValueFullNotOk[128];
    memset(goldValueFullNotOk, 0, 128);
    char goldValueFullOk[128];
    memset(goldValueFullOk, 0, 128);
    char goldValue1[] = "Wolfgang"; // 9
    strcat(goldValueFullNotOk, goldValue1);
    strcat(goldValueFullOk, goldValue1);
    strcat(goldValueFullNotOk, "\r\n");
    strcat(goldValueFullOk, "\r\n");
    int r = logMsg(goldValue1);
    CU_ASSERT(r == 0);
    char goldValue2[] = "Andreas";  // +8 = 17
    strcat(goldValueFullNotOk, goldValue2);
    strcat(goldValueFullOk, goldValue2);
    strcat(goldValueFullNotOk, "\r\n");
    strcat(goldValueFullOk, "\r\n");
    r = logMsg(goldValue2);
    CU_ASSERT(r == 0);
    char goldValue3[] = "Frank";  // +6 = 23
    strcat(goldValueFullNotOk, goldValue3);
    strcat(goldValueFullOk, goldValue3);
    strcat(goldValueFullNotOk, "\r\n");
    strcat(goldValueFullOk, "\r\n");
    r = logMsg(goldValue3);
    CU_ASSERT(r == 0);
    char goldValue4[] = "Thomas";  // +7 = 30
    strcat(goldValueFullNotOk, goldValue4);
    strcat(goldValueFullNotOk, "\r\n");
    r = logMsg(goldValue4);
    CU_ASSERT(r == -1);
    char goldValue5[] = "Barbara";  // +8 = 38, too much
    strcat(goldValueFullNotOk, goldValue5);
    r = logMsg(goldValue5);
    CU_ASSERT(r == -1);

#ifdef DEBUG
    printf("\nRead all chars from logging\n");
#endif
    char buffer[128];
    int c;
    uint8_t i = 0;

    memset(buffer, 0, 128);
    i = 0;
    while (0 < (c = logExec())) {
        buffer[i] = (uint8_t) c;
        i++;
    }
    
    // printf("Buffer: %s\n", buffer);    
    CU_ASSERT(strcmp(goldValueFullNotOk, buffer) != 0);
    CU_ASSERT(strcmp(goldValueFullOk, buffer) == 0);
}
*/


int main() {
    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    CU_pSuite ringbufferSuite = CU_add_suite("Suite_Ringbuffer", init_suite_ringbuffer, clean_suite_ringbuffer);
    if (NULL == ringbufferSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if ((NULL == CU_add_test(ringbufferSuite, "test 0 of ringbuffer, init", testRingbuffer0)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 1 of ringbuffer", testRingbuffer1)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 2 of ringbuffer", testRingbuffer2)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 3 of ringbuffer", testRingbuffer3)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 4 of ringbuffer", testRingbuffer4)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 5 of ringbuffer", testRingbuffer5)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 6 of ringbuffer", testRingbuffer6)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 7 of ringbuffer", testRingbuffer7)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 8 of ringbuffer", testRingbuffer8)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 9 of ringbuffer", testRingbuffer9)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 10 of ringbuffer", testRingbuffer10)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 11 of ringbuffer", testRingbuffer11)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 12 of ringbuffer", testRingbuffer12)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 13 of ringbuffer", testRingbuffer13)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 14 of ringbuffer", testRingbuffer14)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 14 of ringbuffer", testRingbuffer15)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 14 of ringbuffer", testRingbuffer16)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 99 of ringbuffer, free", testRingbuffer99)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 100 of ringbuffer, free", testRingbuffer100)) ||
        (NULL == CU_add_test(ringbufferSuite, "test 101 of ringbuffer, free", testRingbuffer101)) ||
        0 ) {
        CU_cleanup_registry();
        return CU_get_error();
    }

/*
    CU_pSuite loggerSuite = CU_add_suite("Suite_Logger", init_suite_logger, clean_suite_logger);
    if (NULL == loggerSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (
        (NULL == CU_add_test(loggerSuite, "test 0 of logger", testLogger0)) ||
        (NULL == CU_add_test(loggerSuite, "test 1 of logger", testLogger1)) ||
        (NULL == CU_add_test(loggerSuite, "test 2 of logger", testLogger2)) ||
        0 ) {
        CU_cleanup_registry();
        return CU_get_error();
    }
*/

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();
    return CU_get_error();
}