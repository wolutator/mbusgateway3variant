#ifndef _MBUSCOMM_H_
#define _MBUSCOMM_H_

#include <main.h>
#include <stdint.h>
#include <stdbool.h>
#include <config.h>


typedef struct {
  char deviceName[MBUSDEVICE_NAMELENGTH];
  uint8_t address;
  int8_t consideredField[MBUSDEVICE_NUM_OF_CONSIDEREDFIELDS];
  uint32_t requests;
  uint32_t failures;
  int32_t period;
  int32_t delay;
  bool waiting;
  bool active;
} t_mbusDevice;

typedef enum {
  MBCRR_TRIGGERED = 0,
  MBCRR_BUSY = 1,
  MBCRR_DISABLED = 2
} e_mbusCommRequestResult;

typedef struct {
  uint32_t mbusRequestCnt;
  uint32_t mbusErrorCnt;
  uint32_t uartOctetCnt;
  uint32_t uartOverrunCnt;
  uint32_t uartFramingErrCnt;
  uint32_t uartParityErrCnt;
  uint32_t uartNoiseErrCnt;
} t_mbusCommStats;

// e_mbusCommRequestResult mbusCommRequest(t_mbusDevice *mbusDevice);
void mbusCommInit();
void mbusCommExec();
void mbusCommEnable(bool enable);
void mbusCommTxCpltCallback(UART_HandleTypeDef *huart);
void mbusCommRxCpltCallback(UART_HandleTypeDef *huart);
void mbusCommErrorCallback(UART_HandleTypeDef *huart);
void mbusCommSetStats(t_mbusCommStats stats);
t_mbusCommStats *mbusCommGetStats();
void mbusCommAddDevice(t_deviceBlock *deviceBlock);


#endif // _MBUSCOMM_H_
