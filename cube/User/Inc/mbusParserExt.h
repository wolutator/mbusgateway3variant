#ifndef _MBUSPARSEREXT_H_
#define _MBUSPARSEREXT_H_

#include <mbus/mbus-protocol.h>

#include <stdint.h>
#include <stdbool.h>

typedef struct {
    const char *name;
    const char *unit;
    int8_t exponent;
    bool found;
} parsedVIB_t;

parsedVIB_t parseVIB(mbus_value_information_block vib);



#endif //_MBUSPARSEREXT_H_