#ifndef _WIZHELPER_H_
#define _WIZHELPER_H_

#include <stdbool.h>
#include <stdint.h>


int wizInit();
bool isNetworkAvailable();
uint8_t* wizGetIPAddress();
bool wizDnsQuery(char *name, uint8_t *ip);

#endif // _WIZHELPER_H_