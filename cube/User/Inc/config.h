#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <stdint.h>
#include <spi.h>
#include <assert.h>

#define CONFIG_MAGIC 0xdead000a
#define DEVICE_MAGIC 0xaffe0000

typedef struct __attribute__((__packed__)) s_configBlock {
    uint32_t configMagic;
    char deviceName[16];
    uint8_t macAddress[6];
    int32_t frontendThreshold;
    char brokerName[64];
    char watchdogTopic[64];
    char startupTopic[64];
    char statusTopic[64];
    char mbusDataTopic[64];
    char syslogServerName[64];
    uint8_t numOfDeviceBlocks;
    uint8_t filler[1];
} t_configBlock;


#define MAX_MBUS_DEVICES 16
#define MBUSDEVICE_NAMELENGTH 16
#define MBUSDEVICE_NUM_OF_CONSIDEREDFIELDS 4

typedef struct __attribute__((__packed__)) s_deviceBlock {
    uint32_t deviceMagic;
    char deviceName[MBUSDEVICE_NAMELENGTH];
    uint8_t address;
    int8_t consideredField[MBUSDEVICE_NUM_OF_CONSIDEREDFIELDS];
    int32_t period;
    uint8_t filler[3];
} t_deviceBlock;

void configInit();
t_configBlock* getConfig();

#endif /* _CONFIG_H_ */
