#ifndef _CMDHELPER_H_
#define _CMDHELPER_H_

#include <stdint.h>
#include <stdbool.h>


typedef bool (*cmdFunc_t)(uint8_t argc, char **args);

typedef struct {
    char name[24];
    char help[512];
    cmdFunc_t cmdFunc;
} cmd_t;

void sendString(const char *buf);
bool sendFormatString(const char *format, ...);

const cmd_t *getRegularCommands();
const cmd_t *getAdminCommands();
const cmd_t *getConfigCommands();

#endif /* _CMDHELPER_H_ */
