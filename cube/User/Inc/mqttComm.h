#ifndef _MQTTCOMM_H_
#define _MQTTCOMM_H_


void mqttCommInit();
void mqttPublish(const char *topic, char *message);
void mqttPublishf(const char *topic, char *messageFormat, ...);



#endif // _MQTTCOMM_H_


