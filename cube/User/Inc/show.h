#ifndef _SHOW_H_
#define _SHOW_H_


#include <stdint.h>

typedef enum { 
    DEBUG_1 = 0, 
    DEBUG_2 = 1, 
    LED_RED = 2, 
    LED_GREEN = 3
} signalPin_t;

typedef enum { ON, OFF, TOGGLE, BLINK } signalAction_t;

void showInit();
void show(signalPin_t signalPin, signalAction_t action);


#endif // _SHOW_H_
