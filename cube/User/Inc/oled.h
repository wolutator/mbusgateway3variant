/*
 * oled.h
 *
 *  Created on: May 29, 2017
 *      Author: wn
 */

#ifndef OLED_H_
#define OLED_H_


/*
 * Code found at http://www.instructables.com/id/How-to-use-OLED-display-arduino-module/
 * Thank you very much!
 * Adapted from Arduino to STM32 HAL by wollud1969
 */



typedef enum {
    OLED_SCREEN0 = 0,
    OLED_SCREEN1
} oledScreen_t;

void oledInit(void);
void oledClearActiveScreen();
void oledClearAllScreens();
void oledPrint(oledScreen_t screen, char msg[]);
void oledPrintf(oledScreen_t screen, const char *format, ...);
void oledSetActiveScreen(oledScreen_t screen);

#endif /* OLED_H_ */
