#ifndef _LOOPCTRL_H_
#define _LOOPCTRL_H_

#include <stdbool.h>

void loopEnable();
void loopDisable();
void loopStatusCallback();

extern bool loopActive;


#endif // _LOOPCTRL_H_
