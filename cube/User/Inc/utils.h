#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>

void activeDelay(uint8_t delay_ms);

#endif // _UTILS_H_