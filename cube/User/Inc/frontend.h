#ifndef _FRONTEND_H_
#define _FRONTEND_H_

#include <stdint.h>
#include <adc.h>


void frontendInit();
void frontendAdcCallback(ADC_HandleTypeDef* hadc);   
void frontendEnable();
void frontendDisable();

#endif // _FRONTEND_H_