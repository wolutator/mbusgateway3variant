#ifndef _RINGBUFFER_H_
#define _RINGBUFFER_H_

#include <stdint.h>
#include <stdbool.h>


typedef struct {
    uint32_t bufferReadIdx;
    uint32_t bufferWriteIdx;
    uint32_t bufferSize;
    uint8_t* buffer;
} ringbuffer_t;

void ringbufferInit(ringbuffer_t *handle, uint32_t bufferSize);
void ringbufferFree(ringbuffer_t *handle);

int ringbufferPut(ringbuffer_t *handle, uint8_t *data, uint32_t dataLen);
bool ringbufferEmpty(ringbuffer_t *handle);
int ringbufferGetOne(ringbuffer_t *handle); // if positive, cast to uint8_t and be happy, if negative error

// not yet implemented
uint8_t *ringbufferGet(ringbuffer_t *handle, uint32_t dataLen);


#endif // _RINGBUFFER_H_
