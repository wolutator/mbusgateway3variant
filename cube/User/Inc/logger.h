#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <main.h>

#include <stdbool.h>


// Disabling this option is preferred. However, when debugging system hangs
// this option needs to be enabled.
// #define LOGGER_OUTPUT_BY_INTERRUPT

typedef enum {
    LOG_NORMAL,
    LOG_HIGH,
    LOG_RED,
    LOG_GREEN,
    LOG_BLUE,
    LOG_YELLOW
} t_logColor;

// initialize the logger, creates a ringbuffer
void logInit();

// de-initialize the logger, free the ringbuffer
void logFree();

// log a message, make sure it is a null-terminated string
// return value can be ignored, it is only used in test
int logMsg(const char *format, ...);

int coloredMsg(const t_logColor color, bool syslogToo, const char *format, ...);

#ifdef LOGGER_OUTPUT_BY_INTERRUPT
void debugTxCpltCallback(UART_HandleTypeDef *huart);
#endif

#ifndef LOGGER_OUTPUT_BY_INTERRUPT
int logExec();
#endif

#endif // _LOGGER_H_
