#include <wizHelper.h>
#include <stdint.h>
#include <main2.h>
#include <spi.h>
#include <stdbool.h>
#include <logger.h>
#include <PontCoopScheduler.h>
#include <utils.h>
#include <wizchip_conf.h>
#include <string.h>
#include <dhcp.h>
#include <show.h>
#include <oled.h>
#include <dns.h>
#include <config.h>



static t_configBlock *config;

wiz_NetInfo netInfo;

#define DHCP_BUFFER_SIZE 2048
static uint8_t dhcpBuffer[DHCP_BUFFER_SIZE];

#define DNS_BUFFER_SIZE MAX_DNS_BUF_SIZE
static uint8_t dnsBuffer[DNS_BUFFER_SIZE];

extern const uint8_t DHCP_SOCK;
extern const uint8_t DNS_SOCK;

static bool networkAvailable = false;

bool isNetworkAvailable() {
    return networkAvailable;
}

uint8_t* wizGetIPAddress() {
    return netInfo.ip;
}
static void wiz_cs_select(void) {
    HAL_GPIO_WritePin(ETHER_CS_GPIO_Port, ETHER_CS_Pin, GPIO_PIN_RESET);
}

static void wiz_cs_deselect(void) {
    HAL_GPIO_WritePin(ETHER_CS_GPIO_Port, ETHER_CS_Pin, GPIO_PIN_SET);
}

static uint8_t wiz_spi_readbyte(void) {
    uint8_t rbuf;
    HAL_SPI_Receive(&etherSpi, &rbuf, 1, HAL_MAX_DELAY);
    return rbuf;
}

static void wiz_spi_writebyte(uint8_t wb) {
    HAL_SPI_Transmit(&etherSpi, &wb, 1, HAL_MAX_DELAY);
}

static void wiz_spi_readburst(uint8_t* pBuf, uint16_t len) {
    HAL_SPI_Receive(&etherSpi, pBuf, len, HAL_MAX_DELAY);
}

static void wiz_spi_writeburst(uint8_t* pBuf, uint16_t len) {
    HAL_SPI_Transmit(&etherSpi, pBuf, len, HAL_MAX_DELAY);
}

static void wizReset(bool b) {
    HAL_GPIO_WritePin(ETHER_RES_GPIO_Port, ETHER_RES_Pin, b ? GPIO_PIN_RESET : GPIO_PIN_SET);
}


static void wizDHCPAssign() {
    coloredMsg(LOG_BLUE, false, "wizda");
    getIPfromDHCP(netInfo.ip);
    coloredMsg(LOG_BLUE, false, "wizda, IP:  %d.%d.%d.%d", netInfo.ip[0], netInfo.ip[1], netInfo.ip[2], netInfo.ip[3]);
    getGWfromDHCP(netInfo.gw);
    coloredMsg(LOG_BLUE, false, "wizda, GW:  %d.%d.%d.%d", netInfo.gw[0], netInfo.gw[1], netInfo.gw[2], netInfo.gw[3]);
    getSNfromDHCP(netInfo.sn);
    coloredMsg(LOG_BLUE, false, "wizda, SN:  %d.%d.%d.%d", netInfo.sn[0], netInfo.sn[1], netInfo.sn[2], netInfo.sn[3]);
    getDNSfromDHCP(netInfo.dns);
    coloredMsg(LOG_BLUE, false, "wizda, DNS: %d.%d.%d.%d", netInfo.dns[0], netInfo.dns[1], netInfo.dns[2], netInfo.dns[3]);

    wizchip_setnetinfo(&netInfo);
    coloredMsg(LOG_BLUE, false, "wizda, set netinfo again");

    networkAvailable = true;
    show(LED_GREEN, ON);
    coloredMsg(LOG_BLUE, false, "wizda, network is available");

    oledPrintf(OLED_SCREEN0, "Addr:%d.%d.%d.%d", netInfo.ip[0], netInfo.ip[1], netInfo.ip[2], netInfo.ip[3]);
}

static void wizDHCPUpdate() {
    coloredMsg(LOG_BLUE, false, "wizdu");
    getIPfromDHCP(netInfo.ip);
    coloredMsg(LOG_BLUE, false, "wizdu, IP:  %d.%d.%d.%d", netInfo.ip[0], netInfo.ip[1], netInfo.ip[2], netInfo.ip[3]);
    getGWfromDHCP(netInfo.gw);
    coloredMsg(LOG_BLUE, false, "wizdu, GW:  %d.%d.%d.%d", netInfo.gw[0], netInfo.gw[1], netInfo.gw[2], netInfo.gw[3]);
    getSNfromDHCP(netInfo.sn);
    coloredMsg(LOG_BLUE, false, "wizdu, SN:  %d.%d.%d.%d", netInfo.sn[0], netInfo.sn[1], netInfo.sn[2], netInfo.sn[3]);
    getDNSfromDHCP(netInfo.dns);
    coloredMsg(LOG_BLUE, false, "wizdu, DNS: %d.%d.%d.%d", netInfo.dns[0], netInfo.dns[1], netInfo.dns[2], netInfo.dns[3]);

    wizchip_setnetinfo(&netInfo);
    coloredMsg(LOG_BLUE, false, "wizdu, netinfo updated");
}

static void wizDHCPHandler(void *handle) {
    static uint8_t lastDhcpRes = 255;

    uint8_t res = DHCP_run();

    if (lastDhcpRes != res) {
        coloredMsg(LOG_BLUE, false, "wizdh, dhcp state has changed: %d", res);
        lastDhcpRes = res;
    }
}

bool wizDnsQuery(char *name, uint8_t *ip) {
    bool retCode = false;
    coloredMsg(LOG_BLUE, false, "wizdq, querying for %s", name);
    int8_t res = DNS_run(netInfo.dns, (uint8_t*) name, ip);
    coloredMsg(LOG_BLUE, false, "wizdq, DNS_run returns %d", res);
    retCode = (res == 1);
    if (retCode) {
        coloredMsg(LOG_BLUE, false, "wizdq, got address %d.%d.%d.%d", ip[0], ip[1], ip[2], ip[3]);
    }
    return retCode;
}

static void wizPhyLinkHandler(void *handle) {
    // this handler is anyhow called with a 1s period, so we reuse it for the DNS timer
    DNS_time_handler();

    static uint8_t lastStablePhyLink = 255;
    static bool dhcpInitialized = false;

    uint8_t phyLink = 0;
    int8_t res = ctlwizchip(CW_GET_PHYLINK, (void*) &phyLink);
    if (lastStablePhyLink != phyLink) {
        coloredMsg(LOG_BLUE, false, "wizplh, ctlwizchip returns %d, phy link changed to %d", res, phyLink);
        lastStablePhyLink = phyLink;

        if (phyLink == PHY_LINK_ON) {
            oledPrint(OLED_SCREEN0, "Link available");
            // start DHCP handler
            memset(dhcpBuffer, 0, DHCP_BUFFER_SIZE);
            reg_dhcp_cbfunc(wizDHCPAssign, wizDHCPUpdate, NULL);
            DHCP_init(DHCP_SOCK, dhcpBuffer);
            coloredMsg(LOG_BLUE, false, "wizplh, DHCP initialized");

            // run the dhcp handler the first time after 1s and then every 100ms
            schAdd(wizDHCPHandler, NULL, 1000, 1000);
            coloredMsg(LOG_BLUE, false, "wizplh, DHCP handler scheduled");

            dhcpInitialized = true;
        } else {
            oledPrint(OLED_SCREEN0, "Link lost");

            networkAvailable = false;
            show(LED_GREEN, BLINK);
            coloredMsg(LOG_BLUE, false, "wizplh, network is unavailable");

            // stop DHCP handler
            if (dhcpInitialized) {
                DHCP_stop();
                coloredMsg(LOG_BLUE, false, "wizplh, DHCP stopped");

                schDel(wizDHCPHandler, NULL);
                coloredMsg(LOG_BLUE, false, "wizplh, DHCP handler unscheduled");

                dhcpInitialized = false;
            }
        }
    }
}


int wizInit() {
    config = getConfig();
    netInfo.dhcp = NETINFO_DHCP;
    memcpy(netInfo.mac, config->macAddress, 6);

    coloredMsg(LOG_BLUE, false, "wizI, resetting Ethernet module");
    wizReset(true);
    activeDelay(2);
    wizReset(false);
    activeDelay(50);

    coloredMsg(LOG_BLUE, false, "wizI, registering callbacks");
    reg_wizchip_cs_cbfunc(wiz_cs_select, wiz_cs_deselect);
    coloredMsg(LOG_BLUE, false, "wizI, cs funcs registed");
    reg_wizchip_spi_cbfunc(wiz_spi_readbyte, wiz_spi_writebyte);
    coloredMsg(LOG_BLUE, false, "wizI, spi funcs registed");
    reg_wizchip_spiburst_cbfunc(wiz_spi_readburst, wiz_spi_writeburst);
    coloredMsg(LOG_BLUE, false, "wizI, spi burst funcs registed");


    coloredMsg(LOG_BLUE, false, "wizI, initializing Ethernet module");
    uint8_t bufSizes[] = { 2, 2, 2, 2, 2, 2, 2, 2 };
    int8_t res = wizchip_init(bufSizes, bufSizes);
    coloredMsg(LOG_BLUE, false, "wizI, module driver returned %d", res);

    wizphy_reset();
    activeDelay(5);
    coloredMsg(LOG_BLUE, false, "wizI, reset phy");

    wiz_PhyConf wpc;
    wpc.mode = PHY_MODE_MANUAL;
    wpc.speed = PHY_MODE_MANUAL;
    wpc.duplex = PHY_DUPLEX_FULL;
    wizphy_setphyconf(&wpc);
    activeDelay(5);
    coloredMsg(LOG_BLUE, false, "wizI, phy config set");

    wizchip_setnetinfo(&netInfo);
    coloredMsg(LOG_BLUE, false, "wizI, netinfo set to Ethernet module");

    res = wizchip_setnetmode(NM_FORCEARP); // and not NM_PINGBLOCK
    coloredMsg(LOG_BLUE, false, "wizI, set netmode, result is %d", res);

    uint8_t buf[6];
    res = ctlwizchip(CW_GET_ID, (void*) buf);
    coloredMsg(LOG_BLUE, false, "wizI, CW_GET_ID: %d %02x %02x %02x %02x %02x %02x", res, buf[0], buf[1], buf[2], buf[3], buf[4], buf[5]);

    DNS_init(DNS_SOCK, dnsBuffer);

    schAdd(wizPhyLinkHandler, NULL, 0, 1000);
    coloredMsg(LOG_BLUE, false, "wizI, PhyLink handler scheduled");

    return 0;
}
