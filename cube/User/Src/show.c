#include <main.h>
#include <show.h>
#include <PontCoopScheduler.h>

#include <stdint.h>
#include <stdlib.h>

#include <stm32f103xe.h>


typedef struct {
    GPIO_TypeDef *port;
    uint16_t pin;
    signalAction_t currentState;
} showElement_t;

showElement_t showElement[] = {
    { .port = Debug_Signal_1_GPIO_Port, .pin = Debug_Signal_1_Pin, .currentState = OFF },
    { .port = Debug_Signal_2_GPIO_Port, .pin = Debug_Signal_2_Pin, .currentState = OFF },
    { .port = LED_Red_GPIO_Port, .pin = LED_Red_Pin, .currentState = OFF },
    { .port = LED_Green_GPIO_Port, .pin = LED_Green_Pin, .currentState = OFF },
    { .port = NULL, .pin = 0, .currentState = OFF }
};

static void showHandler(void *handle) {
    uint8_t idx = 0;
    while (showElement[idx].port) {
        if (showElement[idx].currentState == BLINK) {
            HAL_GPIO_TogglePin(showElement[idx].port, showElement[idx].pin);
        }
        idx++;
    }
}

void show(signalPin_t signalPin, signalAction_t action) {
    showElement[signalPin].currentState = action;
    if (action == ON || action == OFF) {
        HAL_GPIO_WritePin(showElement[signalPin].port, showElement[signalPin].pin, ((action == ON) ? GPIO_PIN_SET : GPIO_PIN_RESET));
    } else if (action == TOGGLE) {
        HAL_GPIO_TogglePin(showElement[signalPin].port, showElement[signalPin].pin);
    }
}

void showInit() {
    schAdd(showHandler, NULL, 0, 250);
}