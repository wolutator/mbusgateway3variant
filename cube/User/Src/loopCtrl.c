#include <main.h>
#include <loopCtrl.h>
#include <show.h>
#include <logger.h>
#include <oled.h>

bool loopActive = false;

void loopEnable() {
    loopActive = true;
    HAL_GPIO_WritePin(Loop_Enable_GPIO_Port, Loop_Enable_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(Loop_Enable_GPIO_Port, Loop_Enable_Pin, GPIO_PIN_RESET);
    coloredMsg(LOG_HIGH, true, "lc le loop is enabled");
    oledPrint(OLED_SCREEN0, "loop enabled");
}

void loopDisable() {
    HAL_GPIO_WritePin(Loop_Disable_GPIO_Port, Loop_Disable_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(Loop_Disable_GPIO_Port, Loop_Disable_Pin, GPIO_PIN_RESET);
    loopActive = false;
    coloredMsg(LOG_HIGH, true, "lc ld loop is disabled");
    oledPrint(OLED_SCREEN0, "loop disabled");
}

void loopStatusCallback() {
    GPIO_PinState status = HAL_GPIO_ReadPin(Loop_Status_GPIO_Port, Loop_Status_Pin);
    if (status == GPIO_PIN_SET) {
        show(LED_RED, ON);
        loopActive = false;
    }
}