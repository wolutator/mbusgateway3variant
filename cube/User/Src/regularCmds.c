#include <cmdHelper.h>
#include <logger.h>

#include <mbusComm.h>
#include <eeprom.h>


static bool globalStatsCmd(uint8_t argc, char **args) {
    uint32_t uptime = HAL_GetTick() / 1000;
    sendFormatString(\
      "Current uptime:        %ld\n\r" \
      "\n\r",
      uptime
    );

    t_deviceStats *deviceStats = getGlobalDeviceStats();
    sendFormatString(\
      "Global Device statistics\n\r" \
      "  Total running hours: %ld\n\r" \
      "  Total power cycles:  %ld\n\r" \
      "  Total requests:      %ld\n\r" \
      "  Total failures:      %ld\n\r" \
      "\n\r",
      deviceStats->totalRunningHours, deviceStats->totalPowercycles,
      deviceStats->totalRequests, deviceStats->totalFailures
    );

    t_mbusCommStats *mbusStats = mbusCommGetStats();
    sendFormatString(\
      "Global Meterbus/UART statistics\n\r" \
      "  Meterbus Requests:   %ld\n\r" \
      "  Meterbus Errors:     %ld\n\r" \
      "  UART Octets:         %ld\n\r" \
      "  UART Overruns:       %ld\n\r" \
      "  UART Framing Errs:   %ld\n\r" \
      "  UART Parity Errs:    %ld\n\r" \
      "  UART Noise Errs:     %ld\n\r",
      mbusStats->mbusRequestCnt, mbusStats->mbusErrorCnt,
      mbusStats->uartOctetCnt, mbusStats->uartOverrunCnt, mbusStats->uartFramingErrCnt, mbusStats->uartParityErrCnt, mbusStats->uartNoiseErrCnt
    );
    return true;
}



const cmd_t COMMANDS[] = {
    { .name = "globalStats", .cmdFunc = globalStatsCmd,
      .help = \
        "globalStats .......................... Show the global statistics\n\r" \
        "                                       counters requestCnt and errorCnt\n\r"
    },
    { .name = "END_OF_CMDS", .help = "",.cmdFunc = NULL }
};

const cmd_t *getRegularCommands() {
    return COMMANDS;
}