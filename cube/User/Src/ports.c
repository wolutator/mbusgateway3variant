#include <stdint.h>



// on the W5500 there are eight ports available

const uint8_t DHCP_SOCK = 0;
const uint8_t MQTT_SOCK = 1;
const uint8_t CMD_SOCK = 2;
const uint8_t SYSLOG_SOCK = 3;
const uint8_t DNS_SOCK = 4;
