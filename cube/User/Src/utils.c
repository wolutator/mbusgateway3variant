#include <utils.h>
#include <main.h>
#include <stdint.h>


// active waiting, use only during initialization!
void activeDelay(uint8_t delay_ms) {
  uint32_t startTime = HAL_GetTick();
  while (startTime + delay_ms > HAL_GetTick());
}