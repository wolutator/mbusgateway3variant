#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>

#include <main.h>
#include <usart.h>
#include <PontCoopScheduler.h>
#include <mbusComm.h>
#include <loopCtrl.h>
#include <show.h>
#include <logger.h>
#include <frontend.h>
#include <wizHelper.h>
#include <mbusParserExt.h>
#include <mqttComm.h>
#include <oled.h>
#include <ringbuffer.h>
#include <config.h>

#include <mbus/mbus-protocol.h>


static t_configBlock *config;

static const uint8_t MBUS_QUERY_CMD = 0x5b;

// Delay between end of last bit of sent request telegram
// and enabling the frontend for receiving the response.
// Standard says client must wait at least 11 bit times.
// 1 / 2400 * 11 = 4.5e-3ms
static const uint32_t DELAY_AFTER_SENDING = 4; // ms

typedef enum {
    MBCR_SUCCESS = 0,
    MBCR_ERROR_TIMEOUT,
    MBCR_ERROR_LOOP_FAILURE,
    MBCR_ERROR_TX_REG_UNACCESSIBLE,
    MBCR_ERROR_OUT_OF_MEMORY__FRAME,
    MBCR_ERROR_OUT_OF_MEMORY__USERDATA,
    MBCR_ERROR_STATE_ENGINE__START1,
    MBCR_ERROR_STATE_ENGINE__LENGTH1,
    MBCR_ERROR_STATE_ENGINE__LENGTH2,
    MBCR_ERROR_STATE_ENGINE__START2,
    MBCR_ERROR_STATE_ENGINE__INVALID_CHKSUM,
    MBCR_ERROR_STATE_ENGINE__STOP,
    MBCR_ERROR_STATE_ENGINE__ILLEGAL_STATE,
    MBCR_ERROR_STATE_ENGINE__UNKNOWN
} e_mbusCommResult;

typedef enum {
    MBCS_IDLE,
    MBCS_SEND,
    MBCS_SEND_CONTINUED,
    MBCS_SENDING,
    MBCS_SENDING_DONE,
    MBCS_WAIT_AFTER_SENDING,
    MBCS_ENABLE_FRONTEND,
    MBCS_START1,
    MBCS_LENGTH1,
    MBCS_LENGTH2,
    MBCS_START2,
    MBCS_C_FIELD,
    MBCS_A_FIELD,
    MBCS_CI_FIELD,
    MBCS_USERDATA,
    MBCS_CHKSUM,
    MBCS_STOP,
    MBCS_DONE,
    MBCS_TIMEOUT,
    MBCS_DISABLE_FRONTEND,
    MBCS_ERROR,
    MBCS_ERROR_CONTINUED
} e_mbusCommState;

typedef struct {
  uint8_t start1;
  uint8_t length1;
  uint8_t length2;
  uint8_t start2;
  uint8_t l;
  uint8_t c;
  uint8_t a;
  uint8_t ci;
  uint8_t *userdata;
  uint8_t chksum;
  uint8_t stop;
} t_longframe;

typedef struct {
    uint16_t size;
    uint16_t readIdx;
    uint16_t writeIdx;
    uint8_t *buffer;
} linearBuffer_t;

typedef struct {
    uint32_t requestId;
    e_mbusCommState state;
    uint8_t retryCnt;
    uint8_t cmd;
    uint8_t addr;
    linearBuffer_t sendBuffer;
    linearBuffer_t receiveBuffer;
    uint32_t startTime;
    uint8_t receiveCnt;
    bool waitForOctet;
    e_mbusCommResult result;
    t_longframe frame;
    t_mbusDevice *device;
} t_mbusCommHandle;


static t_mbusCommHandle mbusCommHandle = { .requestId = 0, .state = MBCS_IDLE, .retryCnt = 0, .cmd = 0, .addr = 0, .startTime = 0, .receiveCnt = 0, .waitForOctet = false };

static t_mbusCommStats mbusCommStats = { .mbusRequestCnt = 0, .mbusErrorCnt = 0, .uartOctetCnt = 0, .uartOverrunCnt = 0, .uartFramingErrCnt = 0, .uartParityErrCnt = 0, .uartNoiseErrCnt = 0 };

static bool mbusCommEnabled = true;

void mbusCommSetStats(t_mbusCommStats stats) {
    mbusCommStats = stats;
}

t_mbusCommStats *mbusCommGetStats() {
    return &mbusCommStats;
}


static void printError() {
    float errorRatio = ((float) mbusCommHandle.device->failures) / ((float) mbusCommHandle.device->requests);
    coloredMsg(LOG_YELLOW, false, "mbc pe [%d] Error ratio is %.2f", 
               mbusCommHandle.requestId,
               errorRatio);
    mqttPublishf(config->mbusDataTopic, "{\"Status\":\"Error\", \"RequestId\":\"%d\", \"Device\":\"%s\", \"Errors\":\"%d\", \"Requests\":\"%d\", \"ErrorRatio\":\"%.2f\"}", 
                 mbusCommHandle.requestId, mbusCommHandle.device->deviceName,
                 mbusCommHandle.device->failures, mbusCommHandle.device->requests, errorRatio);
    oledPrintf(OLED_SCREEN0, "Err:%d/%d %.2f", mbusCommHandle.device->failures, mbusCommHandle.device->requests, errorRatio);
}



static void parseAndPrintFrame() {
    t_longframe *frame = &(mbusCommHandle.frame);

    mbus_frame reply;
    memset(&reply, 0, sizeof(reply));

    //mbus_parse(&reply, buf, len);
    reply.start1 = frame->start1;
    reply.length1 = frame->length1;
    reply.length2 = frame->length2;
    reply.start2 = frame->start2;
    reply.control = frame->c;
    reply.address = frame->a;
    reply.control_information = frame->ci;
    memcpy(reply.data, frame->userdata, frame->length1 - 3);
    reply.checksum = frame->chksum;
    reply.stop = frame->stop;
    reply.type = MBUS_FRAME_TYPE_LONG;
    reply.data_size = frame->length1 - 3;

    mbus_frame_data frame_data;
    memset(&frame_data, 0, sizeof(frame_data));

    int r = mbus_frame_data_parse(&reply, &frame_data);
    if (r == 0) {
        mbus_data_variable *data_var = &(frame_data.data_var);
        if (data_var->header.status) {
            coloredMsg(LOG_RED, true, "mbc papf [%d %d] sts: %02x", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address,
                       data_var->header.status);
        }
        if ((data_var->header.status & 0x01)) {
            coloredMsg(LOG_RED, true, "mbc papf [%d %d] sts: Application Busy", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address);
        }
        if ((data_var->header.status & 0x02)) {
            coloredMsg(LOG_RED, true, "mbc papf [%d %d] sts: Any Application Error", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address);
        }
        if ((data_var->header.status & 0x04)) {
            coloredMsg(LOG_RED, true, "mbc papf [%d %d] sts: Power Low", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address);
        }
        if ((data_var->header.status & 0x08)) {
            coloredMsg(LOG_RED, true, "mbc papf [%d %d] sts: Permanent Error", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address);
        }
        if ((data_var->header.status & 0x10)) {
            coloredMsg(LOG_RED, true, "mbc papf [%d %d] sts: Temporary Error", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address);
        }
        if ((data_var->header.status & 0x20)) {
            coloredMsg(LOG_RED, true, "mbc papf [%d %d] sts: Specific to manufacturer Error 1", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address);
        }
        if ((data_var->header.status & 0x40)) {
            coloredMsg(LOG_RED, true, "mbc papf [%d %d] sts: Specific to manufacturer Error 2", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address);
        }
        if ((data_var->header.status & 0x80)) {
            coloredMsg(LOG_RED, true, "mbc papf [%d %d] sts: Specific to manufacturer Error 3", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address);
        }
        mbus_data_record *record;
        int i;
        const char *keys[MBUSDEVICE_NUM_OF_CONSIDEREDFIELDS];
        float values[MBUSDEVICE_NUM_OF_CONSIDEREDFIELDS];
        uint8_t numOfConsideredFields = 0;
        for (record = data_var->record, i = 0;
             record;
             record = record->next, i++) {
            for (uint8_t j = 0; j < MBUSDEVICE_NUM_OF_CONSIDEREDFIELDS; j++) {
                if (mbusCommHandle.device->consideredField[j] == i) {
                    parsedVIB_t parsedVIB = parseVIB(record->drh.vib);
                    // coloredMsg(LOG_YELLOW, false, "mbc papf [%d] parsed VIB N: %s, U: %s, E: %d",
                    //            mbusCommHandle.requestId,
                    //            parsedVIB.name, parsedVIB.unit, parsedVIB.exponent);
                    if (parsedVIB.found) {
                        int32_t value = strtol(mbus_data_record_value(record), NULL, 10);
                        float weightedValue = ((float) value) * powf(10.0, ((float) parsedVIB.exponent));
                        coloredMsg(LOG_YELLOW, false, "mbc papf [%d] %s is %.1f %s (%d * 10^%d)",
                                   mbusCommHandle.requestId, parsedVIB.name, weightedValue, parsedVIB.unit,
                                   value, parsedVIB.exponent);
                        keys[numOfConsideredFields] = parsedVIB.name;
                        values[numOfConsideredFields] = weightedValue;
                        numOfConsideredFields++;
                    } else {
                        coloredMsg(LOG_YELLOW, false, "mbc papf [%d] L:%d, VIF: 0x%02x U:%s V:%s", 
                                   mbusCommHandle.requestId,
                                   mbusCommHandle.device->consideredField[j],
                                   record->drh.vib.vif,
                                   mbus_data_record_unit(record),
                                   mbus_data_record_value(record));
                    }
                    
                }
            }
        }
        float errorRatio = ((float) mbusCommHandle.device->failures) / ((float) mbusCommHandle.device->requests);
        coloredMsg(LOG_YELLOW, false, "mbc papf [%d] Error ratio is %.2f", 
                                   mbusCommHandle.requestId,
                                   errorRatio);
        if (numOfConsideredFields == 1) {
            mqttPublishf(config->mbusDataTopic, "{\"Status\":\"Ok\", \"RequestId\":\"%d\", \"Device\":\"%s\", \"Errors\":\"%d\", \"Requests\":\"%d\", \"ErrorRatio\":\"%.2f\", " \
                                     "\"Values\":{\"%s\":\"%.1f\"}}", 
                         mbusCommHandle.requestId, mbusCommHandle.device->deviceName,
                         mbusCommHandle.device->failures, mbusCommHandle.device->requests, errorRatio,
                         keys[0], values[0]);
        } else if (numOfConsideredFields == 2) {
            mqttPublishf(config->mbusDataTopic, "{\"Status\":\"Ok\", \"RequestId\":\"%d\", \"Device\":\"%s\", \"Errors\":\"%d\", \"Requests\":\"%d\", \"ErrorRatio\":\"%.2f\", " \
                                     "\"Values\":{\"%s\":\"%.1f\", \"%s\":\"%.1f\"}}", 
                         mbusCommHandle.requestId, mbusCommHandle.device->deviceName,
                         mbusCommHandle.device->failures, mbusCommHandle.device->requests, errorRatio,
                         keys[0], values[0], keys[1], values[1]);
        } else if (numOfConsideredFields == 3) {
            mqttPublishf(config->mbusDataTopic, "{\"Status\":\"Ok\", \"RequestId\":\"%d\", \"Device\":\"%s\", \"Errors\":\"%d\", \"Requests\":\"%d\", \"ErrorRatio\":\"%.2f\", " \
                                     "\"Values\":{\"%s\":\"%.1f\", \"%s\":\"%.1f\", \"%s\":\"%.1f\"}}", 
                         mbusCommHandle.requestId, mbusCommHandle.device->deviceName,
                         mbusCommHandle.device->failures, mbusCommHandle.device->requests, errorRatio,
                         keys[0], values[0], keys[1], values[1], keys[2], values[2]);
        } else if (numOfConsideredFields == 4) {
            mqttPublishf(config->mbusDataTopic, "{\"Status\":\"Ok\", \"RequestId\":\"%d\", \"Device\":\"%s\", \"Errors\":\"%d\", \"Requests\":\"%d\", \"ErrorRatio\":\"%.2f\", " \
                                     "\"Values\":{\"%s\":\"%.1f\", \"%s\":\"%.1f\", \"%s\":\"%.1f\", \"%s\":\"%.1f\"}}", 
                         mbusCommHandle.requestId, mbusCommHandle.device->deviceName,
                         mbusCommHandle.device->failures, mbusCommHandle.device->requests, errorRatio,
                         keys[0], values[0], keys[1], values[1], keys[2], values[2], keys[3], values[3]);
        }
        oledPrintf(OLED_SCREEN0, "Ok:%d/%d %.2f", mbusCommHandle.device->failures, mbusCommHandle.device->requests, errorRatio);
        mbus_data_record_free(data_var->record);
    } else {
        coloredMsg(LOG_RED, true, "mbc papf [%d %d] err: unable to parse frame", 
                   mbusCommHandle.requestId, mbusCommHandle.device->address);
    }
}



void mbusCommISR() {
    uint32_t isrflags   = READ_REG(mbusUart.Instance->SR);
    uint32_t cr1its     = READ_REG(mbusUart.Instance->CR1);

    // RXNEIE doesn't need to be considered since it is always on and more over the
    // RXNE flag is cleared by reading the DR, which is done in any case
    if (((isrflags & USART_SR_RXNE) != RESET) || ((isrflags & (USART_SR_ORE | USART_SR_FE | USART_SR_PE | USART_SR_NE)) != RESET)) {
        // Error flags are only valid together with the RX flag.
        // They will be cleared by reading SR (already done above) followed by reading DR (below).
        bool errorFound = false;
        if ((isrflags & USART_SR_ORE) != RESET) {
            mbusCommStats.uartOverrunCnt += 1;
            errorFound = true;
        }
        if ((isrflags & USART_SR_FE) != RESET) {
            mbusCommStats.uartFramingErrCnt += 1;
            errorFound = true;
        }
        if ((isrflags & USART_SR_PE) != RESET) {
            mbusCommStats.uartParityErrCnt += 1;
            errorFound = true;
        }
        if ((isrflags & USART_SR_NE) != RESET) {
            mbusCommStats.uartNoiseErrCnt += 1;
            errorFound = true;
        }
        mbusCommStats.uartOctetCnt += 1;
        // it is required to read the DR in any case here, not only when the buffer has space
        // otherwise the interrupt flag won't be reset, particularly important in case of ORE
        uint8_t data = (uint8_t)(mbusUart.Instance->DR & (uint8_t)0x00FF);
        if ((! errorFound) &&
            (mbusCommHandle.receiveBuffer.writeIdx < mbusCommHandle.receiveBuffer.size)) {
            mbusCommHandle.receiveBuffer.buffer[mbusCommHandle.receiveBuffer.writeIdx] = data;
            mbusCommHandle.receiveBuffer.writeIdx += 1;
        }
    } 
    
    // TXEIE needs to be considered since TXE is cleared by writing the DR, which isn't done
    // after the last octet sent
    if (((isrflags & USART_SR_TXE) != RESET) && ((cr1its & USART_CR1_TXEIE) != RESET)) {
        if (mbusCommHandle.sendBuffer.readIdx < mbusCommHandle.sendBuffer.writeIdx) {
            mbusUart.Instance->DR = mbusCommHandle.sendBuffer.buffer[mbusCommHandle.sendBuffer.readIdx];
            mbusCommHandle.sendBuffer.readIdx += 1;
            if (mbusCommHandle.sendBuffer.readIdx == mbusCommHandle.sendBuffer.writeIdx) {
                __HAL_UART_DISABLE_IT(&mbusUart, UART_IT_TXE);
                __HAL_UART_ENABLE_IT(&mbusUart, UART_IT_TC);
            }
        }
    } 

    // advance the state for the engine only when the last octet is shifted out completely
    if (((isrflags & USART_SR_TC) != RESET) && ((cr1its & USART_CR1_TCIE) != RESET)) {
        __HAL_UART_DISABLE_IT(&mbusUart, UART_IT_TC);
        mbusCommHandle.state = MBCS_SENDING_DONE;
    }
}


void mbusCommExec() {
    static uint8_t userdataIdx = 0;
    static uint8_t calculatedChksum = 0;
    uint8_t receivedOctet = 0;
    static uint32_t delayAfterSendStartTime = 0;

    if ((mbusCommHandle.startTime != 0) && ((mbusCommHandle.startTime + 2500) < HAL_GetTick())) {
        mbusCommHandle.state = MBCS_TIMEOUT;
    } else if (mbusCommHandle.waitForOctet) {
        if (mbusCommHandle.receiveBuffer.readIdx >= mbusCommHandle.receiveBuffer.writeIdx) {
            return; // no data available, wait
        }
        receivedOctet = mbusCommHandle.receiveBuffer.buffer[mbusCommHandle.receiveBuffer.readIdx];
        mbusCommHandle.receiveBuffer.readIdx += 1;
        mbusCommHandle.waitForOctet = false;
    }

    switch (mbusCommHandle.state) {
        case MBCS_IDLE:
        // coloredMsg(LOG_YELLOW, false, "mbc hre [%d] state IDLE", mbusCommHandle.requestId);
        break;

        case MBCS_SEND:
        // coloredMsg(LOG_YELLOW, false, "mbc hre [%d] state SEND", mbusCommHandle.requestId);
        mbusCommHandle.sendBuffer.buffer[0] = 0x10;
        mbusCommHandle.sendBuffer.buffer[1] = mbusCommHandle.cmd;
        mbusCommHandle.sendBuffer.buffer[2] = mbusCommHandle.addr;
        mbusCommHandle.sendBuffer.buffer[3] = mbusCommHandle.cmd + mbusCommHandle.addr; // checksum
        mbusCommHandle.sendBuffer.buffer[4] = 0x16;
        mbusCommHandle.sendBuffer.readIdx = 0;
        mbusCommHandle.sendBuffer.writeIdx = 5;
        mbusCommHandle.state = MBCS_SEND_CONTINUED;
        // no break !!

        case MBCS_SEND_CONTINUED:
        // coloredMsg(LOG_YELLOW, false, "mbc hre [%d] state SEND_CONTINUED", mbusCommHandle.requestId);
        show(LED_RED, OFF);
        if (! loopActive) {
            coloredMsg(LOG_YELLOW, true, "mbc hre [%d %d] enabling loop, try %d", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address,
                       mbusCommHandle.retryCnt);
            mbusCommHandle.retryCnt++;
            loopEnable();
        } else {
            mbusCommHandle.retryCnt = 0;
            // enable transmitter interrupt
            //coloredMsg(LOG_YELLOW, false, "mbc hre [%d] enable transmitter interrupt", mbusCommHandle.requestId);
            __HAL_UART_ENABLE_IT(&mbusUart, UART_IT_TXE);
            mbusCommHandle.state = MBCS_SENDING;
        }
        break;

        case MBCS_SENDING:
        // transition from here to MBCS_SENDING_DONE is done by TX ISR
        break;

        case MBCS_SENDING_DONE:
        //coloredMsg(LOG_YELLOW, false, "mbc hre [%d] state SENDING_DONE", mbusCommHandle.requestId);
        delayAfterSendStartTime = HAL_GetTick();
        mbusCommHandle.state = MBCS_WAIT_AFTER_SENDING;
        break;

        case MBCS_WAIT_AFTER_SENDING:
        // Avoids switching on the frontend while there is still noise from sending on the loop
        // Make sure to wait not too long otherwise the beginning of the response is missed
        if ((delayAfterSendStartTime + DELAY_AFTER_SENDING) < HAL_GetTick()) {
            mbusCommHandle.state = MBCS_ENABLE_FRONTEND;
        }
        break;

        case MBCS_ENABLE_FRONTEND:
        // coloredMsg(LOG_YELLOW, false, "mbc hre [%d] state ENABLE_FRONTEND", mbusCommHandle.requestId);
        frontendEnable();
        calculatedChksum = 0;
        userdataIdx = 0;
        mbusCommHandle.receiveBuffer.readIdx = 0;
        mbusCommHandle.receiveBuffer.writeIdx = 0;
        mbusCommHandle.waitForOctet = true; // start receiver
        mbusCommHandle.startTime = HAL_GetTick(); // start receiver timeout
        mbusCommHandle.state = MBCS_START1;
        break;

        case MBCS_START1:
        if (receivedOctet == 0x68) {
            mbusCommHandle.frame.start1 = receivedOctet;
            mbusCommHandle.waitForOctet = true;
            mbusCommHandle.state = MBCS_LENGTH1;
        } else {
            coloredMsg(LOG_RED, true, "mbc hre [%d %d] err: invalid start1 symbol %02x", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address,
                       receivedOctet);
            mbusCommHandle.result = MBCR_ERROR_STATE_ENGINE__START1;
            mbusCommHandle.state = MBCS_ERROR;
        }
        break;

        case MBCS_LENGTH1:
        if (receivedOctet <= 3) {
            coloredMsg(LOG_RED, true, "mbc hre [%d %d] err: length to small %02x", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address,
                       receivedOctet);
            mbusCommHandle.result = MBCR_ERROR_STATE_ENGINE__LENGTH1;
            mbusCommHandle.state = MBCS_ERROR;
        } else {
            mbusCommHandle.frame.length1 = receivedOctet;
            mbusCommHandle.frame.userdata = (uint8_t*) malloc(mbusCommHandle.frame.length1 - 3);
            if (! mbusCommHandle.frame.userdata) {
                coloredMsg(LOG_RED, true, "mbc hre [%d %d] err: unable to allocate memory for userdata", 
                           mbusCommHandle.requestId, mbusCommHandle.device->address);
                mbusCommHandle.result = MBCR_ERROR_OUT_OF_MEMORY__USERDATA;
                mbusCommHandle.state = MBCS_ERROR;
            } else {
                mbusCommHandle.waitForOctet = true;
                mbusCommHandle.state = MBCS_LENGTH2;
            }
        }
        break;

        case MBCS_LENGTH2:
        if (mbusCommHandle.frame.length1 != receivedOctet) {
            coloredMsg(LOG_RED, true, "mbc hre [%d %d] err: invalid length2 %02x vs. %02x", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address,
                       mbusCommHandle.frame.length1, receivedOctet);
            mbusCommHandle.result = MBCR_ERROR_STATE_ENGINE__LENGTH2;
            mbusCommHandle.state = MBCS_ERROR;
        } else {
            mbusCommHandle.frame.length2 = receivedOctet;
            mbusCommHandle.waitForOctet = true;
            mbusCommHandle.state = MBCS_START2;
        }
        break;

        case MBCS_START2:
        if (receivedOctet == 0x68) {
            mbusCommHandle.frame.start2 = receivedOctet;
            mbusCommHandle.waitForOctet = true;
            mbusCommHandle.state = MBCS_C_FIELD;
        } else {
            coloredMsg(LOG_RED, true, "mbc hre [%d %d] err: invalid start2 symbol %02x", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address, receivedOctet);
            mbusCommHandle.result = MBCR_ERROR_STATE_ENGINE__START2;
            mbusCommHandle.state = MBCS_ERROR;
        }
        break;

        case MBCS_C_FIELD:
        mbusCommHandle.frame.c = receivedOctet;
        calculatedChksum += receivedOctet;
        mbusCommHandle.waitForOctet = true;
        mbusCommHandle.state = MBCS_A_FIELD;
        break;

        case MBCS_A_FIELD:
        mbusCommHandle.frame.a = receivedOctet;
        calculatedChksum += receivedOctet;
        mbusCommHandle.waitForOctet = true;
        mbusCommHandle.state = MBCS_CI_FIELD;
        break;

        case MBCS_CI_FIELD:
        mbusCommHandle.frame.ci = receivedOctet;
        calculatedChksum += receivedOctet;
        mbusCommHandle.waitForOctet = true;
        mbusCommHandle.state = MBCS_USERDATA;
        break;

        case MBCS_USERDATA:
        mbusCommHandle.frame.userdata[userdataIdx] = receivedOctet;
        calculatedChksum += receivedOctet;
        userdataIdx++;
        mbusCommHandle.waitForOctet = true;
        if (userdataIdx == (mbusCommHandle.frame.length1 - 3)) {
            mbusCommHandle.state = MBCS_CHKSUM;
        }
        break;

        case MBCS_CHKSUM:
        if (receivedOctet != calculatedChksum) {
            coloredMsg(LOG_RED, true, "mbc hre [%d %d] err: invalid checksum %02x vs %02x", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address,  
                       calculatedChksum, receivedOctet);
            mbusCommHandle.result = MBCR_ERROR_STATE_ENGINE__INVALID_CHKSUM;
            mbusCommHandle.state = MBCS_ERROR;
        } else {
            mbusCommHandle.frame.chksum = receivedOctet;
            mbusCommHandle.waitForOctet = true;
            mbusCommHandle.state = MBCS_STOP;
        }
        break;

        case MBCS_STOP:
        if (receivedOctet == 0x16) {
            mbusCommHandle.frame.stop = receivedOctet;
            mbusCommHandle.state = MBCS_DONE;
        } else {
            coloredMsg(LOG_RED, true, "mbc hre [%d %d] err: invalid stop symbol %02x", 
                       mbusCommHandle.requestId, mbusCommHandle.device->address, receivedOctet);
            mbusCommHandle.result = MBCR_ERROR_STATE_ENGINE__STOP;
            mbusCommHandle.state = MBCS_ERROR;
        }
        break;

        case MBCS_DONE:
        // coloredMsg(LOG_YELLOW, false, "mbc hre [%d] state DONE", mbusCommHandle.requestId);
        parseAndPrintFrame();
        if (mbusCommHandle.frame.userdata != NULL) {
            free(mbusCommHandle.frame.userdata);
            mbusCommHandle.frame.userdata = NULL;
        }
        mbusCommHandle.result = MBCR_SUCCESS;
        mbusCommHandle.state = MBCS_DISABLE_FRONTEND;
        break;

        case MBCS_ERROR:
        // coloredMsg(LOG_RED, false, "mbc hre [%d] state ERROR", mbusCommHandle.requestId);
        coloredMsg(LOG_RED, true, "mbc hre [%d %d] error", mbusCommHandle.requestId, mbusCommHandle.device->address);
        show(LED_RED, ON);
        // disable frontend immediately in case of error since no more data in relevant
        // this avoids strange noise on RX line/led
        frontendDisable();
        mbusCommHandle.state = MBCS_ERROR_CONTINUED;
        // no break

        case MBCS_ERROR_CONTINUED:
        // every error will be collected by a timeout to receive all data still on the wire
        // to avoid leaking old data in responses for new requests
        break;

        case MBCS_TIMEOUT:
        // coloredMsg(LOG_RED, false, "mbc hre [%d] state TIMEOUT", mbusCommHandle.requestId);
        coloredMsg(LOG_RED, true, "mbc hre [%d %d] timeout", mbusCommHandle.requestId, mbusCommHandle.device->address);
        mbusCommStats.mbusErrorCnt += 1;
        mbusCommHandle.device->failures += 1;
        mbusCommHandle.startTime = 0; // disable timeout
        mbusCommHandle.waitForOctet = false; // disable receiver
        printError();
        if (mbusCommHandle.frame.userdata != NULL) {
            free(mbusCommHandle.frame.userdata);
            mbusCommHandle.frame.userdata = NULL;
        }
        // no break

        case MBCS_DISABLE_FRONTEND:
        // coloredMsg(LOG_YELLOW, false, "mbc hre [%d] state DISABLE_FRONTEND", mbusCommHandle.requestId);
        frontendDisable();
        mbusCommHandle.startTime = 0; // disable timeout
        mbusCommHandle.state = MBCS_IDLE;
        break;
        

        default:
        mbusCommHandle.state = MBCS_IDLE;
        break;
    }
}

void mbusCommEnable(bool enable) {
    mbusCommEnabled = enable;
}

static e_mbusCommRequestResult mbusCommRequest(t_mbusDevice *mbusDevice) {
    e_mbusCommRequestResult res = MBCRR_BUSY;

    if (mbusCommEnabled) {
        if (mbusCommHandle.state == MBCS_IDLE) {
            mbusCommHandle.requestId += 1;
            mbusCommHandle.state = MBCS_SEND;
            mbusCommHandle.retryCnt = 0;
            mbusCommHandle.cmd = MBUS_QUERY_CMD;
            mbusCommHandle.addr = mbusDevice->address;
            mbusCommHandle.device = mbusDevice;
            mbusDevice->requests += 1;

            coloredMsg(LOG_YELLOW, false, "mbc mcr [%d] new request %s (%d)", 
                    mbusCommHandle.requestId,
                    mbusDevice->deviceName, mbusDevice->address);

            oledPrint(OLED_SCREEN0, mbusDevice->deviceName);
            res = MBCRR_TRIGGERED;   

            mbusCommStats.mbusRequestCnt += 1;
        }
    } else {
        res = MBCRR_DISABLED;
    }

    return res;
}


#define PERIOD 10

static uint8_t numOfDevices = 0;
static t_mbusDevice *devices = NULL;

void mbusCommAddDevice(t_deviceBlock *deviceBlock) {
    devices = (t_mbusDevice*) realloc((void*) devices, (numOfDevices + 1) * sizeof(t_mbusDevice));
    strcpy(devices[numOfDevices].deviceName, deviceBlock->deviceName);
    devices[numOfDevices].address = deviceBlock->address;
    memcpy(devices[numOfDevices].consideredField, deviceBlock->consideredField, sizeof(deviceBlock->consideredField));
    devices[numOfDevices].period = deviceBlock->period;
    devices[numOfDevices].requests = 0;
    devices[numOfDevices].failures = 0;
    devices[numOfDevices].delay = 0;
    devices[numOfDevices].waiting = false;
    devices[numOfDevices].active = true;
    coloredMsg(LOG_YELLOW, false, "mbc mbcad device added %d %s %d", numOfDevices, devices[numOfDevices].deviceName, devices[numOfDevices].address);
    numOfDevices += 1;
}

/*
static t_mbusDevice devices[] = {
    {
        .deviceName = "Total",
        .address = 80,
        .consideredField = { 0, 17, -1, -1 },
        .requests = 0,
        .failures = 0,
        .period = PERIOD,
        .delay = 0,
        .waiting = false,
        .active = true
    },
    {
        .deviceName = "Computer",
        .address = 85,
        .consideredField = { 0, 4, 2, 3 },
        .requests = 0,
        .failures = 0,
        .period = PERIOD,
        .delay = 0,
        .waiting = false,
        .active = true
    },
    {
        .deviceName = "Dryer",
        .address = 81,
        .consideredField = { 0, 4, 2, 3 },
        .requests = 0,
        .failures = 0,
        .period = PERIOD,
        .delay = 0,
        .waiting = false,
        .active = true
    },
    {
        .deviceName = "Laundry",
        .address = 82,
        .consideredField = { 0, 4, 2, 3 },
        .requests = 0,
        .failures = 0,
        .period = PERIOD,
        .delay = 0,
        .waiting = false,
        .active = true
    },
    {
        .deviceName = "Dishwasher",
        .address = 83,
        .consideredField = { 0, 4, 2, 3 },
        .requests = 0,
        .failures = 0,
        .period = PERIOD,
        .delay = 0,
        .waiting = false,
        .active = true
    },
    {
        .deviceName = "Light",
        .address = 84,
        .consideredField = { 0, 4, 2, 3 },
        .requests = 0,
        .failures = 0,
        .period = PERIOD,
        .delay = 0,
        .waiting = false,
        .active = true
    },
    {
        .deviceName = "Freezer",
        .address = 86,
        .consideredField = { 0, 4, 2, 3 },
        .requests = 0,
        .failures = 0,
        .period = PERIOD,
        .delay = 0,
        .waiting = false,
        .active = true
    },
    {
        .deviceName = "Fridge",
        .address = 87,
        .consideredField = { 0, 4, 2, 3 },
        .requests = 0,
        .failures = 0,
        .period = PERIOD,
        .delay = 0,
        .waiting = false,
        .active = true
    }
};
*/




static void triggerMBusRequest(void *handle) {
    static uint8_t deviceIndex = 0;

    if (devices[deviceIndex].waiting) {
        e_mbusCommRequestResult r = mbusCommRequest(&(devices[deviceIndex]));
        if (r == MBCRR_TRIGGERED) {
            devices[deviceIndex].waiting = false;
            deviceIndex++;
        }
    } else {
        deviceIndex++;
    }
    if (deviceIndex >= numOfDevices) {
        deviceIndex = 0;
    }
}


static void mbusCommScheduler(void *handle) {
    static uint8_t state = 0;

    switch (state) {
        case 0:
            if (isNetworkAvailable()) {
                coloredMsg(LOG_YELLOW, true, "mbc mcs activate scheduler by network");
                schAdd(triggerMBusRequest, NULL, 0, 100);
                state = 2;
            }
            break;

        case 1:
            if (mbusCommEnabled) {
                coloredMsg(LOG_YELLOW, true, "mbc mcs activate scheduler by request");
                schAdd(triggerMBusRequest, NULL, 0, 100);
                state = 2;
            }
            break;

        case 2:
            if (! isNetworkAvailable()) {
                coloredMsg(LOG_YELLOW, true, "mbc mcs deactivate scheduler by network");
                schDel(triggerMBusRequest, NULL);
                loopDisable();
                state = 0;
            }
            if (! mbusCommEnabled) {
                coloredMsg(LOG_YELLOW, true, "mbc mcs deactivate scheduler by request");
                schDel(triggerMBusRequest, NULL);
                loopDisable();
                state = 1;
            }
            for (uint8_t i = 0; i < numOfDevices; i++) {
                if (devices[i].active) {
                    devices[i].delay -= 1;
                    if (devices[i].delay <= 0) {
                        devices[i].delay = devices[i].period;
                        devices[i].waiting = true;
                        coloredMsg(LOG_YELLOW, false, "mbc mcs scheduled: %s", devices[i].deviceName);
                    }
                }
            }

            // FIXME 
            // state = 3;
            break;
        
        case 3:
            coloredMsg(LOG_YELLOW, false, "mbc mcs waiting for godot");
            state = 4;
            // no break

        case 4:
            break;
    }
}

void mbusCommInit() {
    config = getConfig();

    coloredMsg(LOG_GREEN, true, "mbc mci initializing Meterbus communication");

    // enable receive interrupts
//    __HAL_UART_ENABLE_IT(&mbusUart, UART_IT_PE);
//    __HAL_UART_ENABLE_IT(&mbusUart, UART_IT_ERR);
    __HAL_UART_ENABLE_IT(&mbusUart, UART_IT_RXNE);

    // init buffers
    mbusCommHandle.receiveBuffer.size = 256;
    mbusCommHandle.receiveBuffer.buffer = (uint8_t*) malloc(mbusCommHandle.receiveBuffer.size);
    mbusCommHandle.receiveBuffer.readIdx = 0;
    mbusCommHandle.receiveBuffer.writeIdx = 0;

    mbusCommHandle.sendBuffer.size = 8;
    mbusCommHandle.sendBuffer.buffer = (uint8_t*) malloc(mbusCommHandle.sendBuffer.size);
    mbusCommHandle.sendBuffer.readIdx = 0;
    mbusCommHandle.sendBuffer.writeIdx = 0;


    // FIXME
    schAdd(mbusCommScheduler, NULL, 0, 1000);
}
