#include <main.h>
#include <spi.h>
#include <eeprom.h>
#include <string.h>
#include <logger.h>
#include <mbusComm.h>
#include <PontCoopScheduler.h>
#include <utils.h>

#include <assert.h>

#define HIGH GPIO_PIN_SET
#define LOW GPIO_PIN_RESET



static const uint8_t EEPROM_READ = 0x03;
static const uint8_t EEPROM_WRITE = 0x02;
// static const uint8_t EEPROM_WRDI = 0x04;
static const uint8_t EEPROM_WREN = 0x06;
// static const uint8_t EEPROM_RDSR = 0x05;
// static const uint8_t EEPROM_WRSR = 0x01;


static const uint32_t EEPROM_MAGIC = 0xaffe0009;


static const uint16_t EEPROM_HEADER_ADDR = EEPROM_BASE_ADDR;


typedef union {
  t_eepromHeader s;
  uint8_t b[sizeof(struct s_eepromHeader)];
} t_eepromHeaderBlock;

static t_eepromHeaderBlock eepromHeader;


typedef union {
  t_deviceStats s;
  uint8_t b[sizeof(t_deviceStats)];
} t_deviceStatsBlock;

static const uint16_t DEVICE_STATS_ADDR = EEPROM_DEVICE_STATS_ADDR;
static t_deviceStatsBlock deviceStats;

static const uint16_t CONFIG_BLOCK_ADDR = EEPROM_CONFIG_BLOCK_ADDR;
static const uint16_t DEVICE_BLOCK_ADDR = EEPROM_DEVICE_BLOCK_BASE_ADDR;




typedef union {
  struct __attribute__((__packed__)) s_spiMsg {
    uint8_t cmd;
    uint16_t addr;
    uint8_t data[32];
  } s;
  uint8_t b[sizeof(struct s_spiMsg)];
} t_spiMsg;

t_deviceStats* getGlobalDeviceStats() {
  return &(deviceStats.s);
}

inline static void __EEPROM_CS(GPIO_PinState v) {
  HAL_GPIO_WritePin(EEPROM_CS_GPIO_Port, EEPROM_CS_Pin, v);
}

static uint16_t swap(uint16_t i) {
  return ((i & 0x00ff) << 8) | ((i & 0xff00) >> 8);
}


// active waiting, use only during initialization!
static void eepromActiveDelay(uint8_t delay_ms) {
  activeDelay(delay_ms);
}

void eepromWrite(uint16_t addr, uint8_t *buf, uint8_t len) {
  t_spiMsg msg = {
    .s.cmd = EEPROM_WRITE,
    .s.addr = swap(addr)
  };
  memcpy(msg.s.data, buf, len);


  uint8_t writeEnable = EEPROM_WREN;
  __EEPROM_CS(LOW);
  HAL_SPI_Transmit(&eepromSpi, &writeEnable, 1, HAL_MAX_DELAY);
  __EEPROM_CS(HIGH);

  __EEPROM_CS(LOW);
  HAL_SPI_Transmit(&eepromSpi, msg.b, ((uint16_t)(len+3)), HAL_MAX_DELAY);
  __EEPROM_CS(HIGH);
}

void eepromRead(uint16_t addr, uint8_t *buf, uint8_t len) {
  t_spiMsg txMsg = {
    .s.cmd = EEPROM_READ,
    .s.addr = swap(addr)
  };

  t_spiMsg rxMsg;

  __EEPROM_CS(LOW);
  HAL_SPI_TransmitReceive(&eepromSpi, txMsg.b, rxMsg.b, ((uint16_t)(len+3)), HAL_MAX_DELAY);
  __EEPROM_CS(HIGH);

  memcpy(buf, rxMsg.s.data, len);
}

void eepromSpiTxCpltCallback(SPI_HandleTypeDef *hspi) {

}

static void eepromHourlyUpdateDeviceStats(void *handle) {
  deviceStats.s.totalRunningHours += 1;

  t_mbusCommStats *stats = mbusCommGetStats();

  deviceStats.s.totalRequests += stats->mbusRequestCnt;
  deviceStats.s.totalFailures += stats->mbusErrorCnt;

  logMsg("eeHUDS, about to write updated device stats");
  logMsg("eeHUDS, total powercycles so far: %d", deviceStats.s.totalPowercycles);
  logMsg("eeHUDS, total running hours so far: %d", deviceStats.s.totalRunningHours);
  logMsg("eeHUDS, total requests so far: %d", deviceStats.s.totalRequests);
  logMsg("eeHUDS, total failures so far: %d", deviceStats.s.totalFailures);

  eepromWrite(DEVICE_STATS_ADDR, deviceStats.b, sizeof(deviceStats));
}

void eepromReadConfigBlock(t_configBlock *destConfigBlock) {
//  static_assert((sizeof(*destConfigBlock) % EEPROM_WRITE_BLOCK_SIZE == 0), "config block has illegal size, must be dividable by 32");

  for (uint8_t i = 0; i < (sizeof(*destConfigBlock) / EEPROM_WRITE_BLOCK_SIZE); i++) {
    eepromRead(CONFIG_BLOCK_ADDR + (i * EEPROM_WRITE_BLOCK_SIZE), ((uint8_t*)destConfigBlock) + (i * EEPROM_WRITE_BLOCK_SIZE), EEPROM_WRITE_BLOCK_SIZE);
  }
}

void eepromWriteConfigBlock(t_configBlock *srcConfigBlock) {
  for (uint8_t i = 0; i < (sizeof(*srcConfigBlock) / EEPROM_WRITE_BLOCK_SIZE); i++) {
    eepromWrite(CONFIG_BLOCK_ADDR + (i * EEPROM_WRITE_BLOCK_SIZE), ((uint8_t*)srcConfigBlock) + (i * EEPROM_WRITE_BLOCK_SIZE), EEPROM_WRITE_BLOCK_SIZE);
    eepromActiveDelay(EEPROM_AFTER_WRITE_DELAY);
  }
}

void eepromReadDeviceBlock(uint8_t blockNum, t_deviceBlock *destDeviceBlock) {
  static_assert((sizeof(*destDeviceBlock) % EEPROM_WRITE_BLOCK_SIZE == 0), "device block has illegal size, must be dividable by 32");

  for (uint8_t i = 0; i < (sizeof(*destDeviceBlock) / EEPROM_WRITE_BLOCK_SIZE); i++) {
    eepromRead(DEVICE_BLOCK_ADDR + (blockNum * sizeof(*destDeviceBlock)) + (i * EEPROM_WRITE_BLOCK_SIZE), 
               ((uint8_t*)destDeviceBlock) + (i * EEPROM_WRITE_BLOCK_SIZE), 
               EEPROM_WRITE_BLOCK_SIZE);
  }
}

void eepromWriteDeviceBlock(uint8_t blockNum, t_deviceBlock *srcDeviceBlock) {
  for (uint8_t i = 0; i < (sizeof(*srcDeviceBlock) / EEPROM_WRITE_BLOCK_SIZE); i++) {
    eepromWrite(DEVICE_BLOCK_ADDR + (blockNum * sizeof(*srcDeviceBlock)) + (i * EEPROM_WRITE_BLOCK_SIZE), 
                ((uint8_t*)srcDeviceBlock) + (i * EEPROM_WRITE_BLOCK_SIZE), 
                EEPROM_WRITE_BLOCK_SIZE);
    eepromActiveDelay(EEPROM_AFTER_WRITE_DELAY);
  }
}



void eepromInit() {
  __EEPROM_CS(HIGH);

  logMsg("eeI, read header");
  eepromRead(EEPROM_HEADER_ADDR, eepromHeader.b, sizeof(eepromHeader));
  logMsg("eeI, magic: %08x", eepromHeader.s.magic);

  if (eepromHeader.s.magic != EEPROM_MAGIC) {
    logMsg("eeI, eeprom is uninitialized");

    deviceStats.s.totalPowercycles = 0;
    deviceStats.s.totalRunningHours = 0;
    deviceStats.s.totalRequests = 0;
    deviceStats.s.totalFailures = 0;
    logMsg("eeI, about to write device stats for the first time");
    eepromWrite(DEVICE_STATS_ADDR, deviceStats.b, sizeof(deviceStats));
    eepromActiveDelay(EEPROM_AFTER_WRITE_DELAY);

    uint8_t emptyBlock[EEPROM_WRITE_BLOCK_SIZE];
    memset(emptyBlock, 0, sizeof(emptyBlock));
    eepromWrite(CONFIG_BLOCK_ADDR, emptyBlock, EEPROM_WRITE_BLOCK_SIZE);
    eepromActiveDelay(EEPROM_AFTER_WRITE_DELAY);
    logMsg("eeI, config block initialized");

    eepromHeader.s.magic = EEPROM_MAGIC;
    eepromHeader.s.writeCounter = 1;
    logMsg("eeI, about to write header for the first time");
    eepromWrite(EEPROM_HEADER_ADDR, eepromHeader.b, sizeof(eepromHeader));
    eepromActiveDelay(EEPROM_AFTER_WRITE_DELAY);
    logMsg("eeI, eeprom has been initialized");
  } else {
    logMsg("eeI, eeprom is initialized");
  }

  logMsg("eeI, about to read device stats");
  eepromRead(DEVICE_STATS_ADDR, deviceStats.b, sizeof(deviceStats));
  logMsg("eeI, total powercycles so far: %d", deviceStats.s.totalPowercycles);
  logMsg("eeI, total running hours so far: %d", deviceStats.s.totalRunningHours);
  logMsg("eeI, total requests so far: %d", deviceStats.s.totalRequests);
  logMsg("eeI, total failures so far: %d", deviceStats.s.totalFailures);

  deviceStats.s.totalPowercycles += 1;
  logMsg("eeI, about to write device stats with updated power cycles counter");
  eepromWrite(DEVICE_STATS_ADDR, deviceStats.b, sizeof(deviceStats));
  eepromActiveDelay(EEPROM_AFTER_WRITE_DELAY);

  schAdd(eepromHourlyUpdateDeviceStats, NULL, 0, 60 * 60 * 1000);
  logMsg("eeI, hourly device stats update scheduled");
}
