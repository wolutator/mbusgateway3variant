/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define debugUart huart4
#define mbusUart huart5
#define frontendAdc hadc1
#define eepromSpi hspi2
#define etherSpi hspi1
#define displaySpi hspi3
#define debugUartIRQn UART4_IRQn
#define Loop_Enable_Pin GPIO_PIN_2
#define Loop_Enable_GPIO_Port GPIOE
#define Loop_Disable_Pin GPIO_PIN_3
#define Loop_Disable_GPIO_Port GPIOE
#define Loop_Status_Pin GPIO_PIN_4
#define Loop_Status_GPIO_Port GPIOE
#define Loop_Status_EXTI_IRQn EXTI4_IRQn
#define LED_Green_Pin GPIO_PIN_5
#define LED_Green_GPIO_Port GPIOE
#define LED_Red_Pin GPIO_PIN_6
#define LED_Red_GPIO_Port GPIOE
#define Frontend_In_Pin GPIO_PIN_1
#define Frontend_In_GPIO_Port GPIOA
#define ETHER_RES_Pin GPIO_PIN_2
#define ETHER_RES_GPIO_Port GPIOA
#define ETHER_INT_Pin GPIO_PIN_3
#define ETHER_INT_GPIO_Port GPIOA
#define ETHER_CS_Pin GPIO_PIN_4
#define ETHER_CS_GPIO_Port GPIOA
#define ETHER_SCLK_Pin GPIO_PIN_5
#define ETHER_SCLK_GPIO_Port GPIOA
#define ETHER_MISO_Pin GPIO_PIN_6
#define ETHER_MISO_GPIO_Port GPIOA
#define ETHER_MOSI_Pin GPIO_PIN_7
#define ETHER_MOSI_GPIO_Port GPIOA
#define Debug_Signal_2_Pin GPIO_PIN_12
#define Debug_Signal_2_GPIO_Port GPIOE
#define Debug_Signal_1_Pin GPIO_PIN_10
#define Debug_Signal_1_GPIO_Port GPIOB
#define EEPROM_CS_Pin GPIO_PIN_12
#define EEPROM_CS_GPIO_Port GPIOB
#define EEPROM_SCLK_Pin GPIO_PIN_13
#define EEPROM_SCLK_GPIO_Port GPIOB
#define EEPROM_MISO_Pin GPIO_PIN_14
#define EEPROM_MISO_GPIO_Port GPIOB
#define EEPROM_MOSI_Pin GPIO_PIN_15
#define EEPROM_MOSI_GPIO_Port GPIOB
#define Debug_TX_Pin GPIO_PIN_10
#define Debug_TX_GPIO_Port GPIOC
#define Debug_RX_Pin GPIO_PIN_11
#define Debug_RX_GPIO_Port GPIOC
#define MBus_TX_Pin GPIO_PIN_12
#define MBus_TX_GPIO_Port GPIOC
#define Frontend_Out_Pin GPIO_PIN_1
#define Frontend_Out_GPIO_Port GPIOD
#define MBus_RX_Pin GPIO_PIN_2
#define MBus_RX_GPIO_Port GPIOD
#define Display_CS_Pin GPIO_PIN_7
#define Display_CS_GPIO_Port GPIOD
#define Display_SCLK_Pin GPIO_PIN_3
#define Display_SCLK_GPIO_Port GPIOB
#define Display_MISO_Pin GPIO_PIN_4
#define Display_MISO_GPIO_Port GPIOB
#define Display_MOSI_Pin GPIO_PIN_5
#define Display_MOSI_GPIO_Port GPIOB
#define Display_DC_Pin GPIO_PIN_6
#define Display_DC_GPIO_Port GPIOB
#define Display_RES_Pin GPIO_PIN_7
#define Display_RES_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
